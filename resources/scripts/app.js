jQuery(function () {
    var historySupport = !!(window.history && window.history.pushState);
    var appRoot = jQuery("html").data("app");

    initTooltip();

    // SPELLING //

    var showStandardSpellings = false;

    function setSpelling() {
        var nodeList = document.querySelectorAll('tei-w[orig]');

        for (var l = 0; l < nodeList.length; l++) {
            var w = nodeList[l];
            if (showStandardSpellings) {
                w.innerHTML = w.getAttribute('reg');
            }
            else {
                w.innerHTML = w.getAttribute('orig');
            }
        }
    }

    // Word Count Milestone Markers

    var showWordCounts  = true;

    function addWordCounts() {
        var nodeList = document.querySelectorAll('tei-w, tei-pc');
        for (var l = 0; l < nodeList.length; l++) {
            var w = nodeList[l];
            var id = w.getAttribute('id');
            var wordNum = id.split('-').pop();
            if (!isNaN(parseInt(wordNum)) && wordNum % 1000 === 0) {
                w.insertAdjacentHTML('afterend', `<span class="wordcount">${wordNum / 10}</span>`);
            }
        }
    }

    function removeWordCounts() {
        var nodeList = document.querySelectorAll('.wordcount');
        nodeList.forEach(node => {
            node.remove();
        });
    }

    // PAGE NAVIGATION //

    function setNavigationButtonState(button, link, pattern, match) {
        var params = '';
        if (pattern) {
            params += '&pattern=' + pattern;
        }
        if (match) {
            params += '&match=' + match;
        }
        if (!link) {
            return button.removeAttr('href').attr("disabled", true);
        }
        button.attr("href", link+params).removeAttr("disabled");
    }

    jQuery('.viewimage').on("click", function (e) {
        e.preventDefault();
        var facs = jQuery('#facsimile');
        if (facs.hasClass("visually-hidden")) {
            facs.removeClass("visually-hidden");
            jQuery(this).addClass("on");
            jQuery(this).text("Hide images");
        }
        else {
            facs.addClass("visually-hidden");
            jQuery(this).removeClass("on");
            jQuery(this).text("Show images");
        }

    });

    function getFontSize() {
        var size = jQuery("#content-inner").css("font-size");
        return parseInt(size.replace(/^(\d+)px/, "$1"));
    }

    // FACSIMILE //

    var facsimileViewer = document.getElementById('facsimile')

    function getFacsimileUrl() {
        var imageSrcElement = jQuery('[data-facs]');
        if(!imageSrcElement){ return null}
        var facsData = imageSrcElement.data("facs");
        return facsData ? facsData.replace('archivelab.org', 'archive.org').trim() : null;
    }

    function loadFacsimile(facsimileUrl) {
        if (!facsimileViewer) {
            return
        }
        if (!facsimileUrl) {
            hideViewer()
            return console.log('FacsimileViewer:', 'no facsimile url')
        }
        if (facsimileUrl.endsWith("9999")) {
            facsimileViewer.innerHTML = '<div class="wrapper">'
                + '<p/><h4  style="text-align: center; margin: auto; width: 60%;">'
                + 'The image for this page is missing from the image set.'
                + '</h4><p/></div>';
            facsimileViewer.classList.remove('loading');
            return console.log('FacsimileViewer:', 'image missing from image set');
        }


        console.log('FacsimileViewer:', 'load', facsimileUrl)
        showViewer()
        removeFacsimile();
        facsimileViewer.classList.remove('loading')
        var viewer = OpenSeadragon({
            id: "facsimile",
            prefixUrl: "node_modules/openseadragon/build/openseadragon/images/",
            constrainDuringPan: true,
            visibilityRatio: 1,
            minZoomLevel: 1,
            defaultZoomLevel: 1,
            sequenceMode: false,
            showFullPageControl: false,
            showNavigator: true,
            crossOriginPolicy: "Anonymous",
            tileSources: [facsimileUrl + '/info.json']
        });

        viewer.addHandler('open-failed',function(event){
            console.log("failed to open ", facsimileUrl);
        });

        viewer.addHandler('open',function(event){
            console.log("open ", facsimileUrl);
        });

        jQuery('#facsimile').on('open-failed', function (e) {
            console.log("image error: ", e)
        });
    }

    function removeFacsimile() {
        if (!facsimileViewer || !facsimileViewer.hasChildNodes()) {
            return console.log('FacsimileViewer:', 'no facsimile to remove')
        }
        for (var child = facsimileViewer.firstChild; child !== null; child = child.nextSibling) {
            facsimileViewer.removeChild(child);
        }
        facsimileViewer.classList.add('loading')
    }

    function hideViewer() {
        if (!facsimileViewer) {
            return
        }
        facsimileViewer.classList.add('hidden')
    }

    function showViewer() {
        if (!facsimileViewer) {
            return
        }
        facsimileViewer.classList.remove('hidden')
    }

    // ANNOTATIONS //

    var annotationPanel = document.getElementById("annotation-panel");
    var annotationPanelContainer = document.getElementById("annotations");

    if (!annotationPanelContainer || !annotationPanel) {
        let reviewElement = document.querySelector("eplib-review");
        if (reviewElement) {
          annotationPanelContainer = reviewElement.shadowRoot.getElementById("annotations");
          annotationPanel = annotationPanelContainer.querySelector("#annotation-panel");
        }
    }

    var annotationPanelToggle = document.querySelector('.swap.btn');

    if (annotationPanelToggle) {
        annotationPanelToggle.addEventListener('click', function (e) {
            e.preventDefault();
            var classAdded = annotationPanelContainer.classList.toggle('visually-hidden');
            annotationPanelToggle.classList.toggle('on', !classAdded);
        });
    }

    // This is a custom event that will be fired by the close box on the panel.
    window.addEventListener('closeAnnotationPanel', function (e) {
        e.preventDefault();
        hideAnnotationPanelContainer();
    });

    function hideAnnotationPanelContainer () {
        // TODO resize/ reposition document container
        if (!annotationPanelContainer || !annotationPanelToggle) return;
        annotationPanelContainer.classList.add('visually-hidden');
        annotationPanelToggle.classList.remove('on');
    }

    function showAnnotationPanelContainer () {
        // TODO resize/ reposition document container
        if (!annotationPanelContainer || !annotationPanelToggle) return;
        annotationPanelContainer.classList.remove('visually-hidden');
        annotationPanelToggle.classList.add('on');
    }
    window.addEventListener('wordsSelected', showAnnotationPanelContainer, true);

    function setAnnotations(annotationData) {
        // remove loading attribute
        if (!annotationPanel) return;

        annotationPanel.$.list.loading = false;

        if (!annotationData) { return }

        // this is to ensure we have an array
        var annotations = [];
        // single entry
        if (typeof annotationData === 'string' && annotationData.length) {
            annotations = [annotationData]
        }
        // multiple entries
        if (typeof annotationData === 'object' && annotationData && annotationData.length) {
            annotations = annotationData
        }

        // create HTML from strings using jQuery
        var annotationElements = annotations.map(function (annotationHTML) {
            return jQuery(annotationHTML).get(0)
        })

        annotationPanel.$.list.addAnnotations(annotationElements)
    }

    function annopanelDragStart(event) {
        var rect = event.target.getBoundingClientRect();
        event.dataTransfer.setData("text/plain", (rect.left - event.clientX) + ',' + (rect.top - event.clientY));
    }

    function contentDragOver(event) {
        event.preventDefault();
        return false;
    }

    function contentDrop(event) {
        var offset = event.dataTransfer.getData("text/plain").split(',');
        annotationPanelContainer.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
        annotationPanelContainer.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
        event.preventDefault();
        return false;
    }

    if (annotationPanelContainer) {
        annotationPanelContainer.addEventListener("dragstart", annopanelDragStart, true);
    }
    var contentDiv = document.getElementById("content");
    if (contentDiv) {
        contentDiv.addEventListener("dragover", contentDragOver, true);
        contentDiv.addEventListener("drop", contentDrop, true);
    }

    // CONTENT / DOCUMENT //
    function loadPage(params, direction, animate, replaceContent) {
        var animOut = direction == "nav-next" ? "fadeOutLeft" : (direction == "nav-prev" ? "fadeOutRight" : "fadeOut");
        var animIn = direction == "nav-next" ? "fadeInRight" : (direction == "nav-prev" ? "fadeInLeft" : "fadeIn");

        var container = jQuery("#content-container");
        console.log("Loading %s", params);

        jQuery('.tooltip').remove();

        if (annotationPanel
            && annotationPanel.reset
            && typeof annotationPanel.reset === "function") {
            // reset selection
            annotationPanel.reset();
            // clear annotations in list
            annotationPanel.$.list.empty();
            // set loading attribute
            annotationPanel.$.list.loading = true;
        }

        removeFacsimile();

        // prepare parameters to send a query to Blacklab or ajax.xql
        const pathArray = window.location.pathname.split('/');
        const last = pathArray.length - 1
        const doc = pathArray[last].split('&')[0];
        // URLSearchParams does not parse out anything before the ?
        let qparams = new URLSearchParams(params.replace(/^.*\?/, ''));
        const pattern = qparams.get('pattern');
        const matchParam = qparams.get('match');

        // ajax.xql needs the document with extension as a parameter.
        // BlackLab will need it without the extension.
        if (!qparams.has('doc')) {
          qparams.append('doc', doc);
        }

        jQuery.ajax({
            url: appRoot + "/modules/ajax.xql",
            dataType: "json",
            data: qparams.toString(),
            error: function (xhr, status) {
                setAnnotations();
                showContent(container, animIn, animOut);
            },
            success: function (data) {
                jQuery('.tooltip').remove();

                if (data.error) {
                    setAnnotations();
                    showContent(container, animIn, animOut);
                    return;
                }
                if (replaceContent) {
                    jQuery(".content").replaceWith(data.content);
                }
                setAnnotations(data.annotations);

                // fix timing issue in firefox and safari
                // to reliably set selected spelling
                setTimeout(setSpelling, 100);

                if (showWordCounts) addWordCounts();

                if (data.switchView) {
                    jQuery("#switch-view").attr("href", data.switchView);
                    if (data.switchView.indexOf('view=div') !== -1) {
                        jQuery('#switch-view').text('Switch to division view');
                    }
                    else {
                        jQuery('#switch-view').text('Switch to page view');
                    }
                }

                if (data.eeboLink) {
                    jQuery('#eebo-link').attr('href', data.eeboLink).removeAttr('hidden');
                }
                else {
                    jQuery('#eebo-link').removeAttr('href').attr('hidden', true);
                }

                initContent();
                if (animate) {
                    showContent(container, animIn, animOut);
                }

                setNavigationButtonState(toPreviousPageButton, data.previous, pattern, matchParam);
                setNavigationButtonState(toNextPageButton, data.next, pattern, matchParam);

                // for highlighting search hits
                jQuery('.kwic-controls').css('display', 'none'); // will be displayed once loaded
                if (pattern) {
                    const highlighter = document.querySelector('eplib-highlight');
                    if (highlighter) {
                      highlighter.addEventListener('eplib-kwic-load-page', function (e) {
                          e.preventDefault();
                          loadPage( e.detail.url, e.detail.direction, 1, 1);
                          if (historySupport) {
                            history.pushState(null, null, e.detail.url);
                          }
                      });
                    }

                    const event = new CustomEvent("eplib-kwic-load-matches", {
                      detail: {
                        doc: doc.substring(0, doc.lastIndexOf('.')), // strip .xml
                        matchParam: matchParam,
                        pattern: pattern
                      }
                    });
                    highlighter.dispatchEvent(event);
                }
            }
        });
        if (animate) {
            container.addClass("animated " + animOut)
                .one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                    jQuery("#image-container img").css("display", "none");
                });
        }

    }

    function initContent() {
        jQuery(".content .note").popover({
            html: true,
            trigger: "hover"
        });

        loadFacsimile(getFacsimileUrl())
    }

    function showContent(container, animIn, animOut, id) {
        if (!id) {
            window.scrollTo(0, 0);
        }
        container.removeClass("animated " + animOut);
        jQuery("#content-container").addClass("animated " + animIn).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
            jQuery(this).removeClass("animated " + animIn);
            if (id) {
                var target = document.getElementById(id.substring(1));
                target && target.scrollIntoView();
            }
        });
    }

    function isMobile() {
        try {
            document.createEvent("TouchEvent");
            return true;
        }
        catch (e) {
            return false;
        }
    }

    jQuery(".page-nav,.toc-link").on("click", function (ev) {
        console.log("navigation!!!!!!!!!!", this);

        ev.preventDefault();
        if (jQuery(this).attr('disabled') === 'disabled') return;

        var relPath = this.pathname.replace(/^.*\/([^\/]+)$/, "$1");
        var url = "doc=" + relPath + "&" + this.search.substring(1);
        if (historySupport) {
            history.pushState(null, null, this.href);
        }
        loadPage(url, this.className.split(" ")[0], 1, 1);
    });

    var toPreviousPageButton = jQuery('.nav-prev');
    var toNextPageButton = jQuery('.nav-next');

    jQuery("#eplib-zoom-in").on("click", function (ev) {
        ev.preventDefault();
        var size = getFontSize();
        jQuery("#content-inner").css("font-size", (size + 1) + "px");
    });

    jQuery("#eplib-zoom-out").on("click", function (ev) {
        ev.preventDefault();
        var size = getFontSize();
        jQuery("#content-inner").css("font-size", (size - 1) + "px");
    });

    jQuery("#toggle-view-reg").on("click", function (ev) {
        ev.preventDefault();
        if (showStandardSpellings) {
            jQuery(this).text("Show standard spellings");
            showStandardSpellings = false;
        }
        else {
            jQuery(this).text("Show original spellings");
            showStandardSpellings = true;
        }
        jQuery(".download-link").each(function( index, element ) {
            var typeAttr = jQuery(element).attr('data-template-type');
            var hrefAttr = jQuery(element).attr('href');
            var newAttr;
            if (hrefAttr) {
                switch (typeAttr) {
                    case 'plain': // HTML -- use in-app transform
                        newAttr = hrefAttr.replace(/[&\?]standardize=(true|false)$/, '') + '&standardize=' + (showStandardSpellings ? 'true' : 'false');
                        break;
                    default:     // substitution for raw XML and  external e-book downloads
                        newAttr = showStandardSpellings ? hrefAttr.replace(/\./, '_standard.') : hrefAttr.replace(/_standard\./, '.');
                }
                jQuery(element).attr('href', newAttr);
            }
        });
        setSpelling();
    });
    jQuery("#toggle-view-wordcount").on("click", function (ev) {
        ev.preventDefault();
        if (showWordCounts) {
            jQuery(this).text("Show word counts");
            showWordCounts = false;
            removeWordCounts();
        }
        else {
            jQuery(this).text("Hide word counts");
            showWordCounts = true;
            addWordCounts();
        }
    });

    if (isMobile()) {
        jQuery("#content-container").swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                var nav;
                if (direction === "left") {
                    nav = jQuery(".nav-next").get(0);
                } else if (direction === "right") {
                    nav = jQuery(".nav-prev").get(0);
                } else {
                    return;
                }
                var url = "doc=" + nav.pathname.replace(/^.*\/([^\/]+)$/, "$1") + "&" + nav.search.substring(1);
                if (historySupport) {
                    history.pushState(null, null, nav.href);
                }
                loadPage(url, nav.className.split(" ")[0], 1, 1);
            },
            allowPageScroll: "vertical"
        });
    }

    jQuery('.typeahead-meta').autoComplete({
        items: 20,
        minLength: 4,
        resolver: 'custom',
        events: {
            search: function (query, callback, element) {
                var type = element.attr('id');
                jQuery.getJSON("../modules/autocomplete.xql?q=" + query + "&type=" + type, function (data) {
                    callback(data || []);
                });
            }
        },
        formatResult: function (item) {
            if (/[\s,]/.test(item)) {
                return '"' + item + '"';
            }
            return item;
        }
    });

    WebComponents.waitFor(() => {
        // imports are loaded and elements have been registered
        //hide annotation panel
        hideAnnotationPanelContainer()

        var annotationToggle = document.getElementById('toggle-view-annotations')
        if (!annotationToggle) { return } // nothing to toggle

        var docs = [].slice.call(document.querySelectorAll('annotatable-document'))

        var readModeLabel = "Enable annotations"
        var annotationModeLabel = "Disable annotations"
        var annotationsEnabled = !isMobile(); // annotations are enabled by default on non-Mobile

        function enableDisableAnnotations(toggler) {
            docs.forEach(function (doc) {
                doc.highlight = annotationsEnabled;
            })
            if (annotationsEnabled) {
                jQuery(toggler).text(annotationModeLabel);
                return;
            }
            jQuery(toggler).text(readModeLabel);
        }

        enableDisableAnnotations(annotationToggle);

        annotationToggle.addEventListener('click', function (event) {
            event.preventDefault();
            annotationsEnabled = !annotationsEnabled;
            enableDisableAnnotations(annotationToggle);
        })
    });

    function initTooltip() {
        jQuery(".tooltip").remove();
        jQuery('body').tooltip({
            selector: 'tei-w',
            delay: 500,
            html: true,
            container: 'body',
            title: function (el) {
                var lemma = el.getAttribute('lemma');
                var reg = el.getAttribute('reg');
                var pos = el.getAttribute('pos');
                return '<div>' +
                    '<span>id: </span><span>' + el.id + '</span><br/>' +
                    '<span>lemma: </span><span>' + (lemma || 'N/A') + '</span><br/>' +
                    '<span>reg: </span><span>' + (reg || el.original) + '</span><br/>' +
                    '<span style="text-align: left">pos: </span><span>' + (pos || 'N/A') + '</span></div>';
            }
        });
    }

    window.addEventListener('show.bs.tooltip', function (e) {
        var target = e.target;
        jQuery(target).one('hide.bs.tooltip', function () {
            jQuery(this).removeAttr('data-bs-toggle');
        });
        jQuery('#' + target.id).attr('data-bs-toggle', 'tooltip');
    });


    if (document.location.pathname.match(/[^\/]+$/)) {
        var doc = document.location.pathname.match(/[^\/]+$/)[0];
        var search = document.location.search ? document.location.search.replace(/^\?/, '') : '';
        loadPage("doc=" + doc + "&" + search, "nav-next", 0, !search.length);
    }

    var sliderComponent = document.querySelector('eplib-page-slider');
    if (sliderComponent) {
        sliderComponent.addEventListener('eplib-load-page', function (e) {
            e.preventDefault();
            console.log(e);
            var url = "doc=" + e.detail.doc + "&page=" + e.detail.page;
            loadPage( url, "nav-next", 1, 1);
            if (historySupport) {
                var historyUrl = e.detail.doc + "?page=" + e.detail.page;
                history.pushState(null, null, historyUrl);
            }
        });
    }


    // Utilities for form processing.

    function resetForm(form) {
        // clearing inputs
        var inputs = form.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            switch (inputs[i].type) {
                case 'hidden':
                case 'search':
                case 'text':
                    inputs[i].value = '';
                    break;
                case 'radio':
                case 'checkbox':
                    inputs[i].checked = false;
            }
        }

        // clearing selects
        var selects = form.getElementsByTagName('select');
        for (var i = 0; i < selects.length; i++)
            selects[i].selectedIndex = 0;

        // clearing textarea
        var text = form.getElementsByTagName('textarea');
        for (var i = 0; i < text.length; i++)
            text[i].innerHTML= '';

        return false;
    }

    jQuery(".filter_reset").on("click", function (ev) {
        form = ev.target.form;
        resetForm(form);
        form.submit();
    });

    // Handle form control focus changes.

    function _addFormGroupFocus(element) {
        var $element = jQuery(element);
        $element.closest(".form-group").addClass("is-focused");
    }
    function _removeFormGroupFocus(element) {
        jQuery(element).closest(".form-group").removeClass("is-focused"); // remove class from form-group
    }

    jQuery(document).on("focus", ".form-control, .form-group.is-fileinput", function () {
        _addFormGroupFocus(this);
    })
    jQuery(document).on("blur", ".form-control, .form-group.is-fileinput", function () {
        _removeFormGroupFocus(this);
    })


});
