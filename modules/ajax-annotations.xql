xquery version "3.1";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

import module namespace app="http://www.tei-c.org/tei-simple/templates" at "app.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

let $status := request:get-parameter("status", ())
let $creator := request:get-parameter("creator", ())
let $modifier := request:get-parameter("modifier", ())
let $start := request:get-parameter("start", 1)
let $perpage := request:get-parameter("perpage", 10)

let $author := request:get-parameter("author", ())
let $title := request:get-parameter("title", ())
let $date := request:get-parameter("date", ())
let $date2 := request:get-parameter("date2", ())
let $genre := request:get-parameter("genre", ())
let $keyword := request:get-parameter("keyword", ())
let $identifier := request:get-parameter("identifier", ())
let $proofreader := request:get-parameter("proofreader", ())
let $curator := request:get-parameter("curator", ())
let $grade := request:get-parameter("grade", ())
let $gradeop := request:get-parameter("gradeop", ())
let $haspageimages := request:get-parameter("haspageimages", ())
let $pagecount := request:get-parameter("pagecount", ())
let $pagecount2 := request:get-parameter("pagecount2", ())
let $wordcount := request:get-parameter("wordcount", ())
let $wordcount2 := request:get-parameter("wordcount2", ())
let $corpus := request:get-parameter("corpus", ())
let $sortField := request:get-parameter("sortField", ())
let $sortDirection := request:get-parameter("sortDirection", ())

let $start-time := util:system-time()

let $filter :=  app:build-annotation-filter( $creator, $modifier, $status, () )


let $selected :=
    let $filteredDocuments :=
        app:get-filtered-documents(
            $author,
            $title,
            $date,
            $date2,
            $genre,
            $keyword,
            $identifier,
            $proofreader,
            $curator,
            $grade,
            $gradeop,
            $haspageimages,
            $pagecount,
            $pagecount2,
            $wordcount,
            $wordcount2,
            $corpus,
            $sortField,
            $sortDirection,
            false()
        )

    (: If we have documents from the text filter, we'll iterate over those and select
     : annotatations from the related annotation files in an inner loop.  Otherwise
     : we'll iterate over all annotations in the annotation collection that meet
     : the annotation filter criteria, if any.
     :)
    let $query_prologue :=
        if (exists($filteredDocuments))
        then 'for $doc at $position in $filteredDocuments '
                || 'let $annodoc := substring-before(util:document-name($doc), ".xml") || "_annotations.xml" '
                || 'let $annodocpath := $config:annotation-root || "/" || substring($annodoc, 1, 3) || "/" || $annodoc '
                || 'for $anno in doc($annodocpath)'
        else
            'for $anno in collection("' || $config:annotation-root || '")'

    (: The documents are already sorted if a document filter has been specified, so preserve
     : their order by making the position captured in the "at" clause of the outer loop the
     : first order by criterion.
     :)
    let $docOrder := if (exists($filteredDocuments)) then '$position, ' else ''
    (: The rest of the ordering is done by the word ID of where the annotation starts.  That
     : makes adjacent annotations in the list serve as additional context for each other if
     : they are close and in any case seems the most logical sort order.
     :)
    let $order :=
        ' let  $word := if ($anno//target-selector/@type eq "RangeSelector") then $anno//start-selector/@value/string() else $anno//target-selector/@value/string()'
        || ' let $docid := $anno/annotation-target/@source[1]/string()'
        || ' group by ' || $docOrder || ' $word'
        || ' order by ' || $docOrder || ' $word'


    let $query := $query_prologue || '//annotation-item[' || $filter || ']' || $order
                  || ' return let $docid := $anno[1]/annotation-target/@source/string()'
                  || ' return <annotated-word wordid="{$word}" docid="{$docid}" count="{count($anno)}">'
                  || '{for $anno-for-word in $anno return $anno-for-word}</annotated-word>'
    let $x := console:log("$query: " || $query)
    let $result := util:eval($query)
    return $result

let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
let $count := count($selected)
let $x := console:log("Found " || $count || " annotated words in " || $seconds || " seconds.")

return
(
    map
    {
        "count": $count,
        "start": $start,
        "perpage": $perpage,
        "list" : <annotation-list>{fn:subsequence($selected, $start, $perpage)}</annotation-list>
    }
)
