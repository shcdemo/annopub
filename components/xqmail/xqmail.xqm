xquery version "3.0";

module namespace xqmail="http://existsolutions.com/mailmodule";

import module namespace xqmail-config="http://existsolutions.com/mailmodule-config" at "xqmail-config.xqm";
import module namespace mail="http://exist-db.org/xquery/mail" at "java:org.exist.xquery.modules.mail.MailModule";

(: A module to send mail from xquery with support for text and html mails. :)

(:~
    Main function to send mails.

    @param $to the receiver mail address
    @param $cc may contain multiple mail addresses
    @param $bcc blind carbon copy. Might be several addresses
    @param $subject the subject of the mail
    @param $body the text content of the mail body
    @param $html html mail content as element
    @param $attachments mail attachments
:)
declare function xqmail:sendMail($to as xs:string,
        $cc as xs:string?,
        $bcc as xs:string?,
        $subject as xs:string,
        $body as xs:string,
        $html as element()?,
        $attachments ) {
        let $message := xqmail:createMail($to, $cc, $bcc, $subject, $body, $html, $attachments)
        (:let $debug := util:log("INFO", ("Message: ", $message)):)
        let $session := mail:get-mail-session($xqmail-config:SERVER-PROPERTIES)
        return
            mail:send-email($session, $message)
};

declare %private function xqmail:createMail($to, $cc, $bcc, $subject, $body as xs:string, $html as element()?, $attachments ) {
    <mail>
        <from>{$xqmail-config:FROM}</from>
        <to>{$to}</to>
        <cc>{$cc}</cc>
        <bcc>{$bcc}</bcc>
        <subject>{$subject}</subject>
        {
            if($attachments) then $attachments else ()
        }
        <message>
            <text>{$body}</text>
            {
                if ($html) then (
                    <xhtml>{ $html }</xhtml>
                ) else
                    ()
            }
        </message>
    </mail>
};

(: convenience function wrapping some html markup in a body :)
declare function xqmail:create-html-body($subject, $body as element()) {
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>{$subject}</title>
        </head>
        <body>{$body}</body>
    </html>
};

(: convenience function to create an attachment from a resource  :)
declare function xqmail:create-attachment-from-resource($path as xs:string){
    if(util:binary-doc-available($path)) then
        let $doc := util:binary-doc($attachment)
        return
            <attachment filename="{util:document-name($attachment)}" mimetype="{xmldb:get-mime-type($path)}">
                { $doc }
            </attachment>
    else ()
};

(: convenience function to create an attachment directly from content :)
declare function xqmail:create-attachment($filename as xs:string, $mimetype as xs:string, $content){
    if(util:binary-doc-available($path)) then
        let $doc := util:binary-doc($attachment)
        return
            <attachment filename="{$filename}" mimetype="{$mimetype}">
                { $content }
            </attachment>
    else ()
};






