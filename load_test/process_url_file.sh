#!/bin/sh

# This script takes a base URL and a file containing a list of relative
# URLs, one per line, as arguments.
# It fetches each URL with curl and sends the response to /dev/null.

# If the user doesn't supply at least two arguments, print usage note.
if [[ "${#}" -lt 2 ]]
then
  echo "Usage: ${0} <base URL> <URL file>"
  exit 1
fi

BASE_URL="${1}"
URL_FILE="${2}"

# Loop through the URLs in the file.
while read -r LINE
do
    # Skip lines that are commented out.
    [[ "${LINE}" =~ ^#.* ]] && continue
    # Skip lines that are empty.
    [[ -z "${LINE}" ]] && continue
    # Skip lines that are a "plain" (HTML) download.
#    [[ "${LINE}" =~ \.(plain) ]] && continue
    # Skip lines that are PDF or ePub downloads.
#    [[ "${LINE}" =~ \.(pdf|epub) ]] && continue
    # Skip black dot search.
#    [[ "${LINE}" =~ \?query=● ]] && continue
    echo "$URL_FILE: ${BASE_URL}${LINE}"
    # Fetch the URL and throw away the response.
    curl -k -s ${BASE_URL}${LINE} --output /dev/null || echo "Curl failed with $? for ${BASE_URL}${LINE}"

done < ${URL_FILE}
