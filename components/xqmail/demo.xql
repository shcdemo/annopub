xquery version "3.0";

import module namespace xqmail="http://existsolutions.com/mailmodule" at "xqmail.xqm";

let $body-text := "This is a testmail body"
let $html := <p>wow - some html</p>

return xqmail:sendMail("joern@existsolutions.com","joern.turner@betterform.de",(),"test mail",$body-text,$html, ())
