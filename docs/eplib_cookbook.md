# EarlyPrint Library Configuration and Deployment Cookbook <!-- omit in toc -->

This document is called a cookbook because it contains recipes for nearly all of
the many details involved in setting up an instance of the **EarlyPrint
Library**.  A skilled practitioner may choose alternatives to many (most?) of
those details as needed according to local capabilities and requirements, but
what's offered here is a sketch of what's known to work, which incidentally also
serves as a record of the current set-up for the only known instance.  N.B.
There is nothing in this document oriented toward end users.

## Table of Contents <!-- omit in toc -->
- [1. Basic Requirements](#markdown-header-1-basic-requirements)
- [2. Networking and Firewalls](#markdown-header-2-networking-and-firewalls)
- [3. Installing and Configuring Apache](#markdown-header-3-installing-and-configuring-apache)
  - [3.1. Installing Apache and Extensions](#markdown-header-31-installing-apache-and-extensions)
  - [3.2. Basic Apache Configuration](#markdown-header-32-basic-apache-configuration)
  - [3.3. Configuring Apache for HTTPS](#markdown-header-33-configuring-apache-for-https)
  - [3.4. Enabling Apache under SELinux](#markdown-header-34-enabling-apache-under-selinux)
  - [3.5. Directories Used by Apache](#markdown-header-35-directories-used-by-apache)
- [4. Installing eXist](#markdown-header-4-installing-exist)
  - [4.1. Installing OpenJDK](#markdown-header-41-installing-openjdk)
  - [4.2. Considerations When Upgrading eXist](#markdown-header-42-considerations-when-upgrading-exist)
  - [4.3. Creating an eXist User Account](#markdown-header-43-creating-an-exist-user-account)
  - [4.4. Running the eXist JAR Installer](#markdown-header-44-running-the-exist-jar-installer)
  - [4.5. Modifying eXist's conf.xml](#markdown-header-45-modifying-exists-confxml)
  - [4.6. Updating eXist's Memory Settings](#markdown-header-46-updating-exists-memory-settings)
  - [4.7. Modifying the Jetty Session Timeout](#markdown-header-47-modifying-the-jetty-session-timeout)
  - [4.8. Use the Let's Encrypt Certificate in Jetty](#markdown-header-48-use-the-lets-encrypt-certificate-in-jetty)
  - [4.9. Set Ownership on eXist's Home and Data Directories](#markdown-header-49-set-ownership-on-exists-home-and-data-directories)
  - [4.10. Creating a Service Wrapper for eXist](#markdown-header-410-creating-a-service-wrapper-for-exist)
  - [4.11. Start and Monitor eXist](#markdown-header-411-start-and-monitor-exist)
  - [4.12. Set up eXist Backups](#markdown-header-412-set-up-exist-backups)
  - [4.13. Templating Fix-up](#markdown-header-413-templating-fix-up)
- [5. Installing the EarlyPrint Library](#markdown-header-5-installing-the-earlyprint-library)
  - [5.1. Installing External Dependencies](#markdown-header-51-installing-external-dependencies)
  - [5.2. Setting Up eXist User Groups](#markdown-header-52-setting-up-exist-user-groups)
  - [5.3. The Annotation Service](#markdown-header-53-the-annotation-service)
  - [5.4. The Annotations Collection](#markdown-header-54-the-annotations-collection)
  - [5.5. The EarlyPrint Library Application](#markdown-header-55-the-earlyprint-library-application)
  - [5.6. The Texts](#markdown-header-56-the-texts)
  - [5.7. Deploying eXist's Data Directory](#markdown-header-57-deploying-exists-data-directory)
  - [5.8. Restoring Backups](#markdown-header-58-restoring-backups)
  - [5.9. Regenerating Metadata and In-memory Indexes](#markdown-header-59-regenerating-metadata-and-in-memory-indexes)
  - [5.10. Pre-built Downloads](#markdown-header-510-pre-built-downloads)

# 1. Basic Requirements

**Java** or **OpenJDK** and a web server are the most basic requirements to host
the EarlyPrint Library. It requires a server with significant memory, disk
space, and CPU power, and you'll likely want SSD or SAN storage. Most recently
the **Library** has been running on a virtual server with 32GB of RAM, a 1.5TB
data partition, and four Intel Xeon Gold 6238R 2.2 GHz CPU cores.

Many of the build procedures, tools, and set-up instructions in the examples
that follow assume a **Unix** command-line environment, though you may be able
to get it running elsewhere.  You'll need root privileges or sudo access for
many of the steps.

# 2. Networking and Firewalls

Ensure that any firewalls in place on the server or externally allow incoming
access from the internet on ports `443` and `8443`.  You'll also need allow port
`80` for redirection from `http` to `https` to work.  You may want to enable
`8080` for local network traffic, but it is not required.  External firewalls
are beyond the scope of this document (and you may not have control over them
anyway), but enabling ports `80`, `443`, and `8443` locally on your system might
look something like adding lines to `/etc/sysconfig/iptables` with the following
commands:

```sh
$ sudo iptables -A INPUT -p tcp -m multiport --dport 80 -j ACCEPT -m comment --comment "HTTP Access"
$ sudo iptables -A INPUT -p tcp -m multiport --dport 443 -j ACCEPT -m comment --comment "HTTPS Access"
$ sudo iptables -A INPUT -p tcp -m multiport --dport 8443 -j ACCEPT -m comment --comment "Jetty Secure Access"
$ sudo /sbin/service iptables save
```

For site registration to work, you will also need to allow outbound traffic to
a mail server, as well as inbound traffic from that server for the
acknowledgements that are part of the SMTP protocol; this example assumes you
have a mail server called `mail.yourdomain.org`:

```sh
$ sudo iptables -A OUTPUT -s 127.0.0.1 -d mail.yourdomain.org -p tcp -m tcp --dport 587 -m comment --comment "Outgoing mail over TLS" -j ACCEPT
$ sudo iptables -A INPUT -s mail.yourdomain.org -p tcp -m tcp --dport 587 -m comment --comment "Outgoing mail over TLS" -j ACCEPT
```
After you've added your firewall entries, reload iptables with:

```sh
$ sudo systemctl reload iptables.service
```

# 3. Installing and Configuring Apache

## 3.1. Installing Apache and Extensions

The **Apache** web server and the `mod_md` and `mod_ssl` extensions should be
installed and enabled.  `mod_ssl` allows secure connections over HTTPS, and
`mod_md` obtains and renews certificates from the free **Let's Encrypt**
certificate authority.  On **RHEL Linux**, this set-up process looks like the
following:

```sh
$ sudo yum install httpd
$ sudo yum install mod_ssl
$ sudo yum install mod_md
$ sudo systemctl enable httpd
$ sudo systemctl start httpd
```

On **Ubuntu**, these steps would look like the following instead:

```sh
$ sudo apt install apache2
$ sudo a2enmod rewrite
$ sudo a2enmod ssl
$ sudo a2enmod proxy
$ sudo a2enmod proxy_http
$ sudo a2enmod headers
$ sudo a2enmod substitute
$ sudo apt install libapache2-mod-md
$ sudo a2enmod md
$ sudo a2dismod -f autoindex
```

Any time you change the **Apache** configuration, you'll need to restart the
service with:

```sh
$ sudo apachectl restart
```

## 3.2. Basic Apache Configuration

Once Apache is running, you'll need to modify its configuration.  First, edit
`/etc/httpd/conf/httpd.conf`, specifying contact information where the **Let's
Encrypt** certificate renewal bots will contact you, the `mod_md` parameters and
protocols for certificate management, and a `VirtualHost` entry that redirects
port `80` to port `443`.  The following is a GNU unified diff showing example
differences between the stock configuration file and what it should look like
after your modifications:

```diff
-- /etc/httpd/conf/httpd.conf.orig	2023-01-20 12:43:12.621698796 -0600
+++ /etc/httpd/conf/httpd.conf	2023-02-10 13:38:15.753882648 -0600
@@ -86,7 +86,7 @@ Group apache
 # e-mailed.  This address appears on some server-generated pages, such
 # as error documents.  e.g. admin@your-domain.com
 #
-ServerAdmin root@localhost
+ServerAdmin your.email@yourdomain.org

 #
 # ServerName gives the name and port that the server uses to identify itself.
@@ -354,3 +354,21 @@ EnableSendfile on
 #
 # Load config files in the "/etc/httpd/conf.d" directory, if any.
 IncludeOptional conf.d/*.conf
+
+#local additions below
+
+MDomain texts.yourdomain.org
+MDCertificateAgreement accepted
+Protocols h2 http/1.1 acme-tls/1
+
+<VirtualHost *:80>
+    ServerName texts.yourdomain.org
+    ServerAlias servername.myhostingprovider.com
+
+    # Enable rewrite engine and redirect port 80 to port 443.
+
+    RewriteEngine on
+    ReWriteCond         %{SERVER_PORT} !^443$
+    RewriteRule         ^/(.*) https://%{HTTP_HOST}/$1 [NC,R,L]
+</VirtualHost>
+
```

On **Ubuntu**, this same virtual host would go in its own file under
`/etc/apache2/sites-available`. You might as well just call it
`earlyprint.conf`.  To disable the default site and enable this one, run:

```sh
$ sudo a2dissite 000-default.conf
$ sudo a2ensite earlyprint.conf
```

## 3.3. Configuring Apache for HTTPS

There is a second configuration file (`/etc/httpd/conf.d/ssl.conf`) where you'll
need to specify the settings for encrypted connections.  The following diff
shows an example of the changes to this file.  We don't need the default
certificate settings since we're using `mod_md`, but we do need a `VirtualHost`
configuration listening on port `443` that will forward requests appropriately,
notably sending requests intended for **eXist** to the local instance on port
`8080`, but bypasssing the proxy for download requests and requests to
`.well-known/acme-challenge`, which is used to respond to challenges for
certificate renewal. We also rewrite the **eXist** URLs to make them more
meaningful. Finally, we must edit the `Location` header to replace the `http`
scheme with `https`; without this, responses that involve redirects (such as the
login form) will generate mixed content errors.

```diff
--- /etc/httpd/conf.d/ssl.conf.orig	2023-02-17 07:32:00.621705081 -0600
+++ /etc/httpd/conf.d/ssl.conf	2023-02-17 07:38:29.438273664 -0600
@@ -82,7 +82,7 @@ SSLProxyCipherSuite PROFILE=SYSTEM
 #   Some ECC cipher suites (http://www.ietf.org/rfc/rfc4492.txt)
 #   require an ECC certificate which can also be configured in
 #   parallel.
-SSLCertificateFile /etc/pki/tls/certs/localhost.crt
+#SSLCertificateFile /etc/pki/tls/certs/localhost.crt

 #   Server Private Key:
 #   If the key is not combined with the certificate, use this
@@ -90,7 +90,7 @@ SSLCertificateFile /etc/pki/tls/certs/lo
 #   you've both a RSA and a DSA private key you can configure
 #   both in parallel (to also allow the use of DSA ciphers, etc.)
 #   ECC keys, when in use, can also be configured in parallel
-SSLCertificateKeyFile /etc/pki/tls/private/localhost.key
+#SSLCertificateKeyFile /etc/pki/tls/private/localhost.key

 #   Server Certificate Chain:
 #   Point SSLCertificateChainFile at a file containing the
@@ -199,5 +199,47 @@ BrowserMatch "MSIE [2-5]" \
 CustomLog logs/ssl_request_log \
           "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

+# local additions below
+
+    ServerName texts.yourdomain.org
+    ServerAlias servername.myhostingprovider.com
+
+    ServerSignature     off
+
+    ProxyPreserveHost   on
+
+    SSLEngine           on
+    SSLProxyEngine      on
+
+    RequestHeader set X-Forwarded-Proto "https"
+
+    <Proxy *>
+        Order deny,allow
+        Allow from all
+    </Proxy>
+
+    ProxyPass         /.well-known/acme-challenge !
+    ProxyPass         /downloads !
+    ProxyPass         /uploads !
+
+    ProxyPass         /annotation-service/ http://localhost:8080/exist/apps/annotation-service/
+    ProxyPassReverse  /annotation-service/ http://localhost:8080/exist/apps/annotation-service/
+
+    ProxyPass         / http://localhost:8080/exist/apps/shc/ timeout=1200
+    ProxyPassReverse  / http://localhost:8080/exist/apps/shc/ timeout=1200
+
+
+    ProxyPassReverseCookiePath   /exist         /
+
+    RewriteEngine       on
+    RewriteRule         ^/exist/apps/shc/(.*)$     /$1   [PT]
+    RewriteRule         ^/(.*)$     /$1   [PT]
+
+    AddOutputFilterByType INFLATE;SUBSTITUTE;DEFLATE text/html
+
+    Substitute "s|/exist/apps/shc/|/|n"
+    Substitute "s|/exist/apps/annotation-service/|/annotation-service/|n"
+
+    Header edit Location ^http: https:
 </VirtualHost>

```

On **Ubuntu**, this same virtual host would go in its own file under
`/etc/apache2/sites-available`. You might as well just call it
`earlyprint-ssl.conf`. To disable the default site and enable this one, run:

```sh
$ sudo a2dissite default-ssl.conf
$ sudo a2ensite earlyprint-ssl.conf
```

## 3.4. Enabling Apache under SELinux

If running under **SELinux**, the web server will need to be explicitly enabled
as a security exception with the following commands:

```sh
$ sudo ausearch -c 'httpd' --raw | sudo audit2allow -M my-httpd
$ sudo semodule -i my-httpd.pp
```

## 3.5. Directories Used by Apache

You'll need two directories on your data volume, one to serve the downloadable
texts and e-books, and one to function as a staging area for uploading new or
revised texts (or any other large quantity of data that may exceed what you can
put in your home directory). It will be most convenient to have these
directories owned by the account under which you normally log in, but the
downloads should also be world readable so the web server can access them.
Finally, create symbolic links to these directories from the **Apache** web root
directory:

```sh
$ cd /data
$ sudo mkdir downloads
$ sudo mkdir uploads
$ sudo chown -R <username>:<username> downloads
$ sudo chown -R <username>:<username> uploads
$ sudo chmod o+r downloads
$ sudo ln -s /data/uploads /var/www/html/uploads
$ sudo ln -s /data/downloads /var/www/html/downloads
```

# 4. Installing eXist

## 4.1. Installing OpenJDK

Before you can run **eXist**, you need **Java** (or really its open-source
sibling **OpenJDK**). Install it like so:

```sh
$ sudo yum install java-11-openjdk-devel
```

## 4.2. Considerations When Upgrading eXist

For an upgrade, rather than a new install, make sure you have current backups of
the volatile collections (as described in the [backup](#set-up-exist-backups)
section below); then stop the eXist service and move the old exist home
directory out of the way:

```sh
$ sudo systemctl stop eXist-db
$ sudo mv /home/exist/eXist-db /home/exist/eXist-db-6.1.0
```

If you are upgrading from a version that is not binary compatible with the new
version, you should also move the data directory out of the way:

```sh
$ sudo mv /data/existdata /data/existdata_old
```

 The old home and data directories can be used to roll back the upgrade if you
 encounter any problems with it.

 Before upgrading to a new version, check the release notes for anything new or
 special that needs to be done, and consult the [**eXist** Upgrade
 Guide](https://exist-db.org/exist/apps/doc/upgrading).

## 4.3. Creating an eXist User Account

You should not run eXist as `root` but rather create an `exist` user account and
run under that:

```sh
$ sudo adduser exist
```

## 4.4. Running the eXist JAR Installer

Obtain the current stable release of eXist as a jar installer from the
[**eXist** GitHub Releases](https://github.com/eXist-db/exist/releases/) page.
Run the installer as follows:

```sh
$ sudo java -jar exist-installer-6.2.0.jar
```

When prompted, specify "/home/exist/eXist-db" as the install target in answer to
the installer's "Select the installation path" question, and "/data/existdata"
as the data directory in response to the "Data dir" question.  You will also be
prompted for an admin password.

## 4.5. Modifying eXist's conf.xml

You should increase cache sizes and disable the lock table in the configuration
file at `/home/exist/eXist-db/etc/conf.xml`; the following is a unified diff
illustrating what the file will look like after your changes:

```diff
--- /home/exist/eXist-db/etc/conf.xml.orig	2019-08-26 20:54:55.620629201 -0500
+++ /home/exist/eXist-db/etc/conf.xml	2019-08-28 06:54:33.210836984 -0500
@@ -154,9 +154,9 @@
         you have some more memory to waste. If you deal with lots of
         collections, you can also increase the collectionCacheSize value
     -->
-   <db-connection cacheSize="256M"
+   <db-connection cacheSize="1024M"
                   checkMaxCacheSize="true"
-                  collectionCache="64M"
+                  collectionCache="256M"
                   database="native"
                   pageSize="4096"
                   nodesBuffer="1000"
@@ -397,7 +397,7 @@
                 This can also be set via the Java System Properties `org.exist.lock-manager.lock-table.trace-stack-depth`,
                     or (legacy) `exist.locktable.trace.stack.depth`.
         -->
-      <lock-table disabled="false" trace-stack-depth="0"/>
+      <lock-table disabled="true" trace-stack-depth="0"/>
       <!--
             Settings for Collection Locking

```

## 4.6. Updating eXist's Memory Settings

**eXist** is very memory-hungry and the default settings are completely
inadequate to run the **Library**.  On most platforms, there are no
configuration options to control how much memory is allocated to **eXist**, so
we modify the `java` command line in the startup script at
`/home/exist/eXist-db/bin/startup.sh`. On a system with 32GB total memory, we
allocate 26GB to **eXist**, replacing the default `-Xms128m` command switch with
the two switches `-Xms26g -Xmx26g`; these set both minimum and maximum memory to
avoid memory fragmentation. The diff below shows what the revised startup script
looks like:

```diff
--- bin/startup.sh.orig	2019-12-07 12:36:22.526618021 -0600
+++ bin/startup.sh	2019-12-07 12:36:47.179781363 -0600
@@ -117,7 +117,7 @@ if $cygwin; then
   [ -n "$REPO" ] && REPO=`cygpath --path --windows "$REPO"`
 fi

-exec "$JAVACMD" $JAVA_OPTS -Xms128m -Dfile.encoding=UTF-8 -Dlog4j.configurationFile="$BASEDIR"/etc/log4j2.xml -Dexist.home="$BASEDIR" -Dexist.configurationFile="$BASEDIR"/etc/conf.xml -Djetty.home="$BASEDIR" -Dexist.jetty.config="$BASEDIR"/etc/jetty/standard.enabled-jetty-configs \
+exec "$JAVACMD" $JAVA_OPTS -Xms26g -Xmx26g -Dfile.encoding=UTF-8 -Dlog4j.configurationFile="$BASEDIR"/etc/log4j2.xml -Dexist.home="$BASEDIR" -Dexist.configurationFile="$BASEDIR"/etc/conf.xml -Djetty.home="$BASEDIR" -Dexist.jetty.config="$BASEDIR"/etc/jetty/standard.enabled-jetty-configs \
   -classpath "$CLASSPATH" \
   -Dapp.name="startup" \
   -Dapp.pid="$$" \

```

## 4.7. Modifying the Jetty Session Timeout

The **Library** is intended to be an application in which users can work for
hours without interruption, so we need to increase substantially the default
session expiration.  The following diff shows what the modifications to
`/home/exist/eXist-db/etc/jetty/webdefault.xml` should look like:

```diff
--- /home/exist/eXist-db/etc/jetty/webdefault.xml.orig   2017-10-08 13:23:44.431996196 +0000
+++ /home/exist/eXist-db/etc/jetty/webdefault.xml    2017-10-08 13:24:09.908493471 +0000
@@ -343,7 +343,7 @@
   <!-- Default session configuration                                        -->
   <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
   <session-config>
-    <session-timeout>30</session-timeout>
+    <session-timeout>480</session-timeout>
   </session-config>

   <!-- ==================================================================== -->
   
```

## 4.8. Use the Let's Encrypt Certificate in Jetty

**eXist** serves requests via ports `8080` and `8443` using the **Jetty** web
server embedded in it.  User traffic will hit port `8080` via the **Apache**
proxy set up previously.  But to run **eXist's** admin interface securely, it is
necessary to make the certificate obtained by **Apache's** `mod_md` extension
available to **Jetty** so it can handle secure requests on port `8443`.  To do
this we convert the certificate to **PKCS12** format and copy the resulting
keystore to **eXist's** **Jetty** directory.  Start by creating a file called
`create_keystore.sh` in any convenient (secure) location on the server with the
following contents, substituting an actual password of your choosing for the
string "\<password\>":

```sh
#!/bin/sh

# Convert the private key and certificate to a PKCS12 file

SERVER=texts.yourdomain.org

openssl pkcs12 -export \
		-in /etc/httpd/state/md/domains/${SERVER}/pubcert.pem \
		-inkey /etc/httpd/state/md/domains/${SERVER}/privkey.pem \
		-out /etc/httpd/state/md/domains/${SERVER}/keystore.p12 \
		-name jetty \
		-passout pass:<password>

# Copy new keystore to jetty.

cp -f /etc/httpd/state/md/domains/${SERVER}/keystore.p12 \
			/home/exist/eXist-db/etc/jetty/keystore.p12
```

Make the file executable and run it:

```sh
$ chmod +x create_keystore.sh
$ sudo ./create_keystore.sh
```

Now edit the file `/home/exist/eXist-db/etc/jetty/jetty-ssl-context.xml` and
change the entries for `KeyStorePassword`, `KeyManagerPassword`, and
`TrustStorePassword` to the password you set when generating the keystore.

Finally, **Jetty** must either be told what name to look for in the certificate
provided in the keystore, or told not to require the "existdb" name. The latter
is easiest, and involves simply removing the three lines that look like the
following from `jetty-ssl-context.xml`.

```xml
   <Set name="CertAlias">
      <Property name="jetty.keystore.alias" default="existdb"/>
   </Set>
```

## 4.9. Set Ownership on eXist's Home and Data Directories

We installed **eXist** as `root` and have been modifying its configuration with
`sudo`, so before we start it for the first time, we need to make the `exist`
user the owner of its home and data directories like so:

```sh
$ sudo chown -hR exist:exist /data/existdata
$ sudo chown -hR exist:exist /home/exist/eXist-db
```

## 4.10. Creating a Service Wrapper for eXist

To run **eXist** as a service, we need to create a service wrapper for it.  On
servers running `systemd` this should be as simple as creating a file at
`/etc/systemd/system/eXist-db.service` and placing the following contents in it:

```ini
[Unit]
Description=eXist-db – Native XML Database –
Documentation=http://exist-db.org/exist/apps/doc/
After=syslog.target

[Service]
Type=simple
User=exist
Group=exist
ExecStart=/home/exist/eXist-db/bin/startup.sh
Restart=no
StandardOutput=syslog
StandardError=syslog
PrivateTmp=yes
PrivateDevices=yes
ProtectSystem=full
NoNewPrivileges=yes

[Install]
WantedBy=multi-user.target
```

## 4.11. Start and Monitor eXist

Now we are finally ready to enable and start the **eXist** service with the
following commands:

```sh
$ sudo systemctl enable eXist-db
$ sudo systemctl start eXist-db
```

You shouldn't need to enable the service more than once, but for various
maintenance activities you will need to stop it:

```sh
$ sudo systemctl stop eXist-db
```
and/or restart it:

```sh
$ sudo systemctl restart eXist-db
```

You can monitor **eXist's** recent activity and troubleshoot problems by viewing
its log file like so:

```sh
$ sudo tail -500 /home/exist/eXist-db/logs/exist.log
```

## 4.12. Set up eXist Backups

Backing up the security and annotation collections can be done with a simple
script.  First create a `backups` directory on the data volume and navigate to
it:

```sh
$ cd /data
$ sudo mkdir backups
$ cd backups
```

Now create a file called `backup_exist.sh` with the following contents:

```sh
#/bin/sh

start_time=`date +%s`

/home/exist/eXist-db/bin/backup.sh -u admin -p <admin-password> -b /db/system/security/exist -d /data/backups/exist_security_$(date +%Y%m%d_%H%M%S).zip
/home/exist/eXist-db/bin/backup.sh -u admin -p <admin-password> -b /db/apps/EarlyPrintAnnotations -d /data/backups/EarlyPrintAnnotations_$(date +%Y%m%d_%H%M%S).zip
find /data/backups -type f -name '*.zip' -mtime +30 -exec rm {} \;

end_time=`date +%s`
echo execution time was `expr $end_time - $start_time` s.
```

Be sure to substitue "\<admin-password\>" with your actual admin password.  This
example script creates backups of the annotation and user security collections
with the current timestamp in the filename and purges any backups older than 30
days.

Now make the backup script executable and protect it since it contains a
password:


```sh
$ sudo chmod +x backup_exist.sh
$ sudo chmod o-rwx backup_exist.sh

```
You'll then want to set up a `cron` job to run the backup script.  Start by
creating a staging file called `exist.cron.tmp` containing the crontab entries
that look like the following:

```crontab
0 5 * * * /data/backups/backup_exist.sh > /data/backups/backup.log 2>&1
0 20 * * * /data/backups/backup_exist.sh > /data/backups/backup.log 2>&1
```

In this example, the backup job is set to run at 5 AM and 8 PM every day. Next,
load the crontab entries into the `exist` user's crontab like so, and check to
see what that entry looks like:

```sh
$ sudo crontab -u exist exist.cron.tmp
$ sudo crontab -l -u exist
0 5 * * * /data/backups/backup_exist.sh > /data/backups/backup.log 2>&1
0 20 * * * /data/backups/backup_exist.sh > /data/backups/backup.log 2>&1
$
```

Finally,  make the `exist` user account the owner for the entire `backups`
directory:

```sh
$ cd ..
$ sudo chown -hR exist:exist backups
```

For additional information on backing up and restoring with **eXist**, see the
[official documentation](https://exist-db.org/exist/apps/doc/backup.xml) or the
notes below on [restoring](#restoring-backups) from one of the backups described
here.

## 4.13. Templating Fix-up

The standard templating library that comes with **eXist** allows a maximum of 20
URL parameters, which is not enough for the **Library** application.  Change the
default `MAX_ARITY` from 20 to 30 in the file at
`/data/existdata/expathrepo/templating-1.1.0/content/templates.xqm` (or the file
for whatever version of this library that came with your version of **eXist**):

```diff
--- /data/existdata/expathrepo/templating-1.1.0/content/templates.xqm.orig	2023-02-19 17:59:56.307210544 -0600
+++ /data/existdata/expathrepo/templating-1.1.0/content/templates.xqm	2023-02-19 20:37:20.883095687 -0600
@@ -31,7 +31,7 @@ declare variable $templates:NOT_FOUND :=
 declare variable $templates:TOO_MANY_ARGS := QName("http://exist-db.org/xquery/html-templating", "TooManyArguments");
 declare variable $templates:PROCESSING_ERROR := QName("http://exist-db.org/xquery/html-templating", "ProcessingError");
 declare variable $templates:TYPE_ERROR := QName("http://exist-db.org/xquery/html-templating", "TypeError");
-declare variable $templates:MAX_ARITY := 20;
+declare variable $templates:MAX_ARITY := 30;

 declare variable $templates:ATTR_DATA_TEMPLATE := "data-template";

```

# 5. Installing the EarlyPrint Library

Most of the installations described here involve building **XAR** files on a
development machine (which might as well be the developer's laptop) and then
loading the file having the `.xar` extension using the **Package Manager** that
is part of **eXist's** dashboard. For more on this way of doing things, see
**eXist's** [Package Repository](https://exist-db.org/exist/apps/doc/repo)
documentation.

The 65,000+ documents served up by the **Library** represent a special case that
doesn't work well as a **XAR** installation for several reasons, not least
because moving all of the texts from one machine to another means moving well
over 100GB of data, even when compressed. It is simply not practical to install
a `.xar` file containing the number and size of files in the **Library**, so
there are various utilities to load some or all of the texts. The locality of
the build machine that prepares the texts for loading and the server that will
serve them up via the **EarlyPrint Library** application may become important,
depending on whatever network speeds you have available.

## 5.1. Installing External Dependencies

Log in to the **eXist** dashboard as `admin` and install "TEI Publisher:
 Processing Model Libraries" and "Memsort XQuery Module" from the "Available"
tab of the **Package Manager**.  Or you may wish to do the same thing from the
command line by creating a file called `eplib_prereqs.xql` with the following
contents:

```xql
repo:install-and-deploy("http://existsolutions.com/apps/tei-publisher-lib",
                        "2.10.1",
                        "http://exist-db.org/exist/apps/public-repo/find"),
repo:install-and-deploy("http://exist-db.org/xquery/memsort",
                        "http://exist-db.org/exist/apps/public-repo/find")
```

and then running the commands in that file with:

```sh
$ sudo /home/exist/eXist-db/bin/client.sh \
       --user admin \
      --password <admin_password> \
      --no-gui \
      --file eplib_prereqs.xql
```

## 5.2. Setting Up eXist User Groups

In **eXist** (using the **Java Admin Client** or the dashboard's **User
Manager**) create the groups `shcadmin` and `shcuser`. Also add the `shcadmin`
user account and make it a member of the `shcadmin` group.  If you add
application users manually, add them to the `shcuser` group when you create
them. Registrations will be added to that group automatically. Users who need
the ability to approve corrections should be added to `shcadmin` manually.

## 5.3. The Annotation Service

Build and install the annotation service from [its git
repository](https://bitbucket.org/shcdemo/annotation-service/src/master/). You
can install it from the **Package Manager** in the **eXist** dashboard, or you
can move it into the autodeploy directory and restart **eXist** as follows:

```sh
$ sudo mv annotation-service-1.4.8.xar /home/exist/eXist-db/autodeploy/
$ sudo chown exist:exist /home/exist/eXist-db/autodeploy/annotation-service-1.4.8.xar
$ sudo systemctl restart eXist-db
```

You may need to check the status of the autodeployment by looking at the
`expath-repo` log file:

```sh
$ sudo cat /home/exist/eXist-db/logs/expath-repo.log
```

## 5.4. The Annotations Collection

If you have an existing annotations collection, _back it up first!_. In fact, if
you have a backup, it is not normally necessary to install again; just restore
the backup.  But to create annotations for a first-time installation, or to
create a new empty annotations collection (removing any existing annotations),
build and install from the ["build annotations" git
repository](https://bitbucket.org/shcdemo/build_annotations/src/master/),
following the instructions in its
[README.md](https://bitbucket.org/shcdemo/build_annotations/src/master/README.md)
to install it from the **Package Manager**.  Alternatively, it may be installed
by placing its **XAR** file in the `autodeploy` directory as described above for
the [**Annotation Service**](#the-annotation-service). This will result in a
`/db/apps/EarlyPrintAnnotations` collection within your **eXist** database.

## 5.5. The EarlyPrint Library Application

Clone, build, and install the EarlyPrint Library application from [its  git
repository](https://bitbucket.org/shcdemo/annopub/src/master/), following the
instructions in its
[README.md](https://bitbucket.org/shcdemo/annopub/src/master/README.md).
Alternatively, it may be installed by placing its **XAR** file in the
`autodeploy` directory as described above for the [**Annotation
Service**](#the-annotation-service).

## 5.6. The Texts

After first creating the file `../admin.pwd` containing the password to the
`shcadmin` account, clone, build and install an empty texts collection from the
["build texts" git
repository](https://bitbucket.org/shcdemo/build_texts/src/master/), following
the instructions in its
[README.md](https://bitbucket.org/shcdemo/build_texts/src/master/README.md).
This results in an empty `/db/apps/EarlyPrintTexts` collection within your
**eXist** database with indexing preferences configured.

Obtain the texts by cloning the git repositories for the
[EEBO](https://bitbucket.org/eplib/eebotcp/src/master/),
[ECCO](https://bitbucket.org/eplib/eccotcp/src/master/), and
[Evans](https://bitbucket.org/shcdemo/evanstexts/src/master/) corpora and using
the instructions in the README.md file in each repository.  Because of the
number and size of the files in these repositories, they use git submodules,
which complicates the set-up process -- do read and follow the instructions
carefully. The text repositories will require over 175GB of disk space and take
some hours to clone, even with fast networking and storage.  You almost
certainly do _not_ want the various steps that load and process the texts to
take place on your production server as these steps will consume all machine
resources for days at a time; we recommend a development machine with the same
specs as the production server and ideally on the same network to facilitate
moving large quantities of data from the development to the production machine.

To ease the difficulty of navigating three separate repositories, there is a
fourth repository called
[eptexts](https://bitbucket.org/eplib/eptexts/src/master/) that sits alongside
the three actual text repositories and provides symbolic links to the files in
them. This virtual repository may then be treated as a single large one; see its
[README.md](https://bitbucket.org/eplib/eptexts/src/master/README.md) for
details.

Once again using the ["build texts" git
repository](https://bitbucket.org/shcdemo/build_texts/src/master/), build and
load the texts following the instructions in the
[README.md](https://bitbucket.org/shcdemo/build_texts/src/master/README.md)
there, which also covers loading a subset of texts, such as those most recently
updated with corrections.  If you load all of the texts, the loading will likely
take at least two or three days on a powerful server.  You will need at least
100GB of additional disk space as a staging area over and above the requirements
for the repository, and of course wherever your eXist data reside you will be
storing the same data all over again plus indexes.

## 5.7. Deploying eXist's Data Directory

The mechanisms described in the previous section represent the tried and true
method for shoehorning the texts into **eXist**, but there are several reasons
that you may not want to run these processes directly on your production server,
including:

1. You are loading a large number of texts (or maybe all of them) and would like
to avoid downtime for end users while loading.
2. Sometimes **eXist** detects an "unclean" shutdown and decides to reindex all
collections.  This takes several days (or longer) to run and renders the system
unusable while running.  It's actually faster to install everything again from
scratch than to wait for reindexing to complete.
3. You are upgrading to a new version of **eXist** that is not binary
compatible with the previous version and need to avoid days of downtime while
waiting for **eXist's** backup and restore operations.
4. Something bad happened to your production server and you need to get up and
running from a warm standby without starting from scratch.

All these reasons for downtime can be avoided by doing a wholesale "lift and
shift" of the **eXist** data directory from a development server to the
production server. Here's how to do that.

The development server should be running the same version of **eXist** that the
production server will be running by the time the data migration is complete.
It should also be on the same platform and have an up-to-date load of all the
texts.

Shut down **eXist** on the development server and create a zip archive of the
entire data directory, running the zip operation in the background since it will
take several hours (maybe seven or eight) to complete:

```sh
$ sudo systemctl shutdown eXist-db
$ cd /data
$ sudo zip -r uploads/existdata.zip existdata/ 2>&1 > uploads/zip.log &
```
The zip archive will be about 115GB, down from over 500GB unzipped.

On the production server, pull the zip archive over and unpack it:

```sh
$ scp username@dev.yourdomain.org:/data/uploads/existdata.zip /data/uploads/
$ cd /data/uploads
$ unzip existdata.zip
```

On the production system, if it's still running, take a last-minute backup of
the security and and annotation collections:

```sh
$ cd /data/backups
$ sudo -u exist ./backup_exist.sh
```

On the production system, shut down **eXist**, move the current data directory
out of the way, move the new data directory into place, set its ownership, and
restart **eXist**:

```sh
$ sudo systemctl stop eXist-db
$ sudo mv /data/existdata /data/existdata_old
$ sudo mv /data/uploads/existdata /data/existdata
$ sudo chown -hR exist:exist /data/existdata
$ sudo systemctl start eXist-db
```

[Restore the latest backups](#restoring-backups) of the security and annotations
collections and [regenerate
metadata](#regenerating-metadata-and-in-memory-indexes).  You now have an
up-to-date **eXist** instance after only minutes instead of days of downtime.


## 5.8. Restoring Backups

It may be necessary on occasion to restore backups of the annotation or user
security collection. This is easily done from the **Java Admin Client**, or from
the command line like so:

```sh
$ sudo /home/exist/eXist-db/bin/backup.sh \
    --user admin --password <admin_password> \
    --rebuild --restore \
    /data/backups/exist_security_20230219_050001.zip
```

In this example, the file `exist_security_20230219_050001.zip` is the most
recent backup of the security database found in the local backups directory.

## 5.9. Regenerating Metadata and In-memory Indexes

Any time the texts are updated, there is a metadata index that needs to be
regenerated.  This process will also update the in-memory index used by the
text list.  This task may be performed by logging in to the application as a
user with administrator priveleges and choosing "Update Document Metadata Index"
from the "Admin" menu, or by running the following from the command line:

```sh
$ curl -H 'Content-Type: application/xml' \
     --user admin:<admin_password> \
     'http://localhost:8080/exist/rest/db/apps/shc/modules/index.xql'
```

## 5.10. Pre-built Downloads

The **Library** serves up pre-built downloads of the texts in `ePub` and `PDF`
formats as well as zipped versions of the source **XML** files, all provided in
both original and standard spelling. The tools to generate these files are in
the [downloads](https://bitbucket.org/eplib/downloads/src/master/) git
repository; consult its
[README.md](https://bitbucket.org/shcdemo/build_texts/src/master/README.md) for
details.  If serving these files from the application, configure your web proxy
to make them available at the `/downloads` URL as described above in the
[Configuring Apache for HTTPS](#configuring-apache-for-https) section.

The generation of these downloads will produce over 500,000 files that occupy
close to 100GB of storage.  It takes several days or even a week to produce
them and consumes all available CPU, memory, and storage bandwidth while running.
As a result, it's best to generate them on a separate server and pull them to
the downloads folder of the production server when complete.  We do this with `rsync`
using a command similar to the following on the production server:

```sh
$ rsync -avrz --delete \
        username@dev.yourdomain.org:/data/downloads/ \
        /data/downloads
```

Even with very large pipes, this may take half an hour or more, but when it's
complete there are no further post-processing steps as these files are simply
served up as-is.