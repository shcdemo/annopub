xquery version "3.1";

(:~
 : Non-standard extension functions, mainly used for the documentation.
 :)
module namespace pmf="http://pib.at.northwestern.edu/apps/shc/html-behaviours";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace html="http://www.tei-c.org/tei-simple/xquery/functions";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

declare function pmf:webcomponent($config as map(*), $node as element(), $class as xs:string+, $content, $space-after as xs:boolean?) {
    let $id := $node/@xml:id
    return
        element { "tei-" || local-name($node) } {
            $node/@* except $id,
            if ($id) then
                attribute id { $id }
            else
                (),
            attribute class { $class },
            $config?apply-children($config, $node, $content)
        },
        if ($space-after) then
            element tei-c { " " }
        else
            ()
};

declare function pmf:webcomponent-orig($config as map(*), $node as element(), $class as xs:string+, $content, $space-after as xs:boolean?) {

    let $id := $node/@xml:id
    let $match := if ($node/@exist:match) then 'match' else ''
    return
        element { "tei-" || local-name($node) } {
            $node/@* except $id,
            if ($id) then
                attribute id { $id }
            else
                (),
            attribute class { $class, $match },
            if ($node/@reg and $node/@reg != $node/text() and count($node/@orig) = 0) then
                attribute orig { $node/text() }
            else
                (),
            if ($node/@orig and $node/@orig != $node/text() and count($node/@reg) = 0) then
                attribute reg { $node/text() }
            else
                (),
            $config?apply-children($config, $node, $content)
        },
        if ($space-after) then
            element tei-c { " " }
        else
            ()
};

declare function pmf:c($config as map(*), $node as element(), $class as xs:string+, $content) {
    <tei-c>{$content}</tei-c>
};

declare function pmf:pb-img($config as map(*), $node as element(), $class as xs:string+, $content, $type as xs:string, $label as item()*) {
    let $id := $node/@xml:id

    return 
    <span>
        {if($content) then
            attribute data-facs { 
                    $config?apply-children($config, $content, $content) }
            else
                (),
            if ($id) then
                attribute id { $id }
            else
                (),
            attribute class { $class },
            $config?apply-children($config, $node, $label)
        }
    </span>
};

declare function pmf:characters($content as item()) {
    html:escapeChars(normalize-space($content))
};