# user self registration

A module to allow users to register with an application.

## registration flow

1. application provides a link to a self-registration form. This is implemented as a Polymer web component
1. user fills fields 'email' and 'password' and clicks 'register'
1. request will be stored on the server which will generate a token
1. server sends email containing confirmation link containing the token
1. server will create appropriate account with email address as user. 

## configuration

All configurable params are found in self-registration-config.xqm