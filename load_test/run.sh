#/bin/sh

start_time=`date +%s`

for f in *.t
    do
        echo "Processing $f"
        ./process_url_file.sh ''https://devtexts.earlyprint.org/exist/apps/shc' ${f} &
    done

wait

end_time=`date +%s`
echo execution time was `expr $end_time - $start_time` seconds
