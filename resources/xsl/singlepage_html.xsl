<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs tei" version="2.0">

    <!-- Retrieve standardize parameter.-->

    <xsl:param name="standardize"/>

    <!-- Suppress excess spaces. -->

    <xsl:strip-space elements="*"/>

    <!-- Default identity templates. -->

    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- Eject most of TEI header section. -->

    <xsl:template match="tei:fileDesc">
    </xsl:template>
    <xsl:template match="tei:profileDesc">
    </xsl:template>
    <xsl:template match="tei:titleDesc">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:editionStmt">
    </xsl:template>
    <xsl:template match="tei:publicationStmt">
    </xsl:template>
    <xsl:template match="tei:sourceDesc">
    </xsl:template>
    <xsl:template match="tei:revisionDesc">
    </xsl:template>
    <xsl:template match="tei:biblFull">
    </xsl:template>

    <!-- Eject  machine-generated_castlist and
            textual_notes divs.
    -->

    <xsl:template match="tei:div">
        <xsl:choose>
            <xsl:when test="@type='machine-generated_castlist' or @type= 'textual_notes'">
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*, node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Eject facsimile and xenoData sections. -->

    <xsl:template match="tei:facsimile">
    </xsl:template>

    <xsl:template match="tei:xenoData">
    </xsl:template>

    <!-- Drop xml:id except for a few places where it might be useful. -->

    <xsl:template match="@xml:id">
        <xsl:choose>
            <xsl:when test="parent::tei:TEI or parent::tei:pb or parent::tei:ref or parent::tei:note">
                <xsl:copy>
                    <xsl:apply-templates select="."/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

      <!-- If <pc> needs a following space, add one, and drop empty pc elements. -->

    <xsl:template match="tei:pc">
        <xsl:choose>
            <xsl:when test="text()=''"> <!-- Drop empty pc elements -->
            </xsl:when>
            <xsl:when test="not(@join = 'right' or @join = 'both') and following::*[1] and not(parent::tei:q/tei:pc[last()] = node()) and not(parent::tei:quote/tei:pc[last()] = node())">
                <xsl:apply-templates select="text()"/>
                <xsl:choose>
                    <xsl:when test="(parent::tei:hi/tei:pc)[last()] = node() or following::*[1][self::tei:hi]">
                        <xsl:element name="c">
                            <xsl:text> </xsl:text>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text> </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="text()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- If <w> needs a following space, add one.
         Pick @reg over content when standard spellings have been chosen.
    -->

    <xsl:template match="tei:w">
        <xsl:choose>
            <xsl:when test="@reg and $standardize='true'">
                 <xsl:value-of select="@reg"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="text()"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="not(@join = 'right' or @join = 'both') and not(following::*[1][self::tei:w and (@join = 'left' or @join='both')]) and not(following::*[1][self::tei:pc and not(@join='right')]) and not(parent::tei:q/tei:w[last()] = node()) and not(parent::tei:quote/tei:w[last()] = node())">
            <xsl:choose>
                <xsl:when test="(parent::tei:hi/tei:w)[last()] = node() or following::*[1][self::tei:hi]">
                    <xsl:element name="c">
                        <xsl:text> </xsl:text>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text> </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
