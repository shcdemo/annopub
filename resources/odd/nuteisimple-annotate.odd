<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="tei-pm.nvdl"
  type="application/xml"
  schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:ep="http://earlyprint.org/ns/1.0" xml:lang="en">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>TEI Simple customization</title>
            </titleStmt>
            <publicationStmt>
                <publisher>TEI Consortium</publisher>
                <availability>
                    <licence target="http://creativecommons.org/licenses/by-sa/3.0/"> Distributed
                        under a Creative Commons Attribution-ShareAlike 3.0 Unported License </licence>
                    <licence target="http://www.opensource.org/licenses/BSD-2-Clause">
                        <p>Copyright 2014 TEI Consortium.</p>
                        <p>All rights reserved. </p>
                        <p>Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are
                            met:</p>
                        <list>
                            <item>Redistributions of source code must retain the above copyright
                                notice, this list of conditions and the following disclaimer.</item>
                            <item>Redistributions in binary form must reproduce the above copyright
                                notice, this list of conditions and the following disclaimer in the
                                documentation and/or other materials provided with the
                                distribution.</item>
                        </list>
                        <p>This software is provided by the copyright holders and contributors "as
                            is" and any express or implied warranties, including, but not limited
                            to, the implied warranties of merchantability and fitness for a
                            particular purpose are disclaimed. In no event shall the copyright
                            holder or contributors be liable for any direct, indirect, incidental,
                            special, exemplary, or consequential damages (including, but not limited
                            to, procurement of substitute goods or services; loss of use, data, or
                            profits; or business interruption) however caused and on any theory of
                            liability, whether in contract, strict liability, or tort (including
                            negligence or otherwise) arising in any way out of the use of this
                            software, even if advised of the possibility of such damage.</p>
                    </licence>
                    <p>TEI material can be licensed differently depending on the use you intend to
                        make of it. Hence it is made available under both the CC+BY and BSD-2
                        licences. The CC+BY licence is generally appropriate for usages which treat
                        TEI content as data or documentation. The BSD-2 licence is generally
                        appropriate for usage of TEI content in a software environment. For further
                        information or clarification, please contact the <ref target="mailto:info@tei-c.org">TEI Consortium</ref>. </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>created ab initio during a meeting in Oxford</p>
            </sourceDesc>
        </fileDesc>
    </teiHeader>
    <text>
        <body>
            <schemaSpec ident="myteisimple" start="TEI teiCorpus" source="teisimple.odd">
                <elementSpec mode="change" ident="head">
                    <model predicate="parent::figure" behaviour="block">
                        <outputRendition>font-style: italic;</outputRendition>
                    </model>
                    <model predicate="parent::table" behaviour="block">
                        <outputRendition>font-style: italic;</outputRendition>
                    </model>
                    <model predicate="parent::lg" behaviour="block">
                        <outputRendition>font-style: italic;</outputRendition>
                    </model>
                    <model predicate="parent::list" behaviour="block">
                        <outputRendition>font-weight: bold;</outputRendition>
                    </model>
                    <model predicate="not(parent::div/ancestor::div)" behaviour="heading">
                        <outputRendition> color: #983722; border-bottom: 1px solid #983722;
                            margin-bottom: .5em; padding-bottom: .25em; </outputRendition>
                    </model>
                    <model predicate="parent::div" behaviour="heading"/>
                    <model behaviour="block"/>
                </elementSpec>
                <elementSpec mode="change" ident="add">
                    <model behaviour="block">
                        <outputRendition>color: red; text-decoration: underline;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec ident="name" mode="change">
                    <attList>
                        <attDef ident="type" mode="change">
                            <valList mode="replace" type="closed">
                                <valItem ident="person"/>
                                <valItem ident="forename"/>
                                <valItem ident="surname"/>
                                <valItem ident="organisation"/>
                                <valItem ident="object"/>
                                <valItem ident="country"/>
                                <valItem ident="place"/>
                            </valList>
                        </attDef>
                    </attList>
                    <model behaviour="block">
                        <outputRendition>color: blue;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="choice">
                    <constraintSpec ident="choiceSize" scheme="isoschematron" mode="add">
                        <constraint>
                            <assert xmlns="http://purl.oclc.org/dsdl/schematron" test="count(*) &gt; 1" role="ERROR"> Element "<sch:name xmlns:sch="http://purl.oclc.org/dsdl/schematron"/>" must have at
                                least two child elements.</assert>
                        </constraint>
                    </constraintSpec>
                    <constraintSpec ident="choiceContent" scheme="isoschematron" mode="add">
                        <constraint>
                            <assert xmlns="http://purl.oclc.org/dsdl/schematron" test="(tei:corr or tei:sic or tei:expan or     tei:abbr or tei:reg or tei:orig) and ((tei:corr and tei:sic) or (tei:expan     and tei:abbr) or (tei:reg and tei:orig))" role="ERROR"> Element "<sch:name xmlns:sch="http://purl.oclc.org/dsdl/schematron"/>" must have
                                corresponding corr/sic, expand/abbr, reg/orig </assert>
                        </constraint>
                    </constraintSpec>
                    <model output="plain" predicate="sic and corr" behaviour="inline">
                        <param name="content">corr[1]</param>
                    </model>
                    <model output="plain" predicate="abbr and expan" behaviour="inline">
                        <param name="content">expan[1]</param>
                    </model>
                    <model output="plain" predicate="orig and reg" behaviour="inline">
                        <param name="content">reg[1]</param>
                    </model>
                    <model predicate="sic and corr" behaviour="alternate">
                        <param name="default">corr[1]</param>
                        <param name="alternate">sic[1]</param>
                    </model>
                    <model predicate="abbr and expan" behaviour="alternate">
                        <param name="default">expan[1]</param>
                        <param name="alternate">abbr[1]</param>
                    </model>
                    <model output="epub" predicate="orig and reg" behaviour="inline">
                        <param name="content">orig[1]</param>
                    </model>
                    <model output="web" predicate="orig and reg" behaviour="webcomponent">
                        <param name="default">orig[1]</param>
                        <param name="alternate">reg[1]</param>
                    </model>
                    <model predicate="orig and reg" behaviour="alternate">
                        <param name="default">orig[1]</param>
                        <param name="alternate">reg[1]</param>
                    </model>
                    <model predicate="unclear" behaviour="alternate">
                        <param name="default">if (unclear[@cert="high"]) then unclear[@cert="high"]
                            else unclear[1]</param>
                        <param name="alternate">unclear[@cert!="high"]</param>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="c">
                    <model output="web" behaviour="c">
                        <param name="content">node()</param>
                    </model>
                    <model behaviour="text"/>
                </elementSpec>
                <elementSpec mode="change" ident="graphic">
                    <model behaviour="inline">
                        <param name="content">@url</param>
                    </model>
                </elementSpec>
               <elementSpec mode="change" ident="lg">
                    <model behaviour="block">
                        <outputRendition>margin-top: 0.5em; margin-bottom: 0.5em;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="note">
                    <model predicate="@place='bottom'" behaviour="note">
                        <param name="place">"margin"</param>
                    </model>
                    <model predicate="@place" behaviour="note">
                        <param name="place">@place</param>
                    </model>
                    <model predicate="parent::div and not(@place)" behaviour="block">
                        <outputRendition>margin-left: 10px;margin-right: 10px; font-size:smaller;</outputRendition>
                    </model>
                    <model predicate="not(@place)" behaviour="inline">
                        <outputRendition scope="before">content:" [";</outputRendition>
                        <outputRendition scope="after">content:"] ";</outputRendition>
                        <outputRendition>font-size:small;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="pb">
                    <model predicate="@facs and @xml:id and not(@type = 'blank') and not(@type='duplicate')" behaviour="pb-img" cssClass="page-facsimile">
                        <param name="content" value="if($parameters?root) then id(concat(@xml:id, '-im'), $parameters?root) else id(concat(@xml:id, '-im'))"/>
                        <param name="type">'page'</param>
                        <param name="label">(concat(if(@n) then concat('Page ', @n,' ') else '',if(@xml:id) then concat('Image ', substring-after(@xml:id, '-')) else ''))</param>
                        <outputRendition>
                            display: block;
                            margin-left: 4pt;
                            color: grey;
                            float: right;
                            font-size: medium;
                            font-style: normal;
                        </outputRendition>
                        <outputRendition scope="before">content: '[';</outputRendition>
                        <outputRendition scope="after">content: ']';</outputRendition>
                    </model>
                    <model behaviour="break" useSourceRendition="true">
                        <param name="type">'page'</param>
                        <param name="label">(concat(if(@n) then concat('Page ', @n,' ') else '',if(@facs) then concat('Image ', @facs) else ''))</param>
                        <outputRendition>
                            display: block;
                            margin-left: 4pt;
                            color: grey;
                            float: right;
                            font-size: medium;
                            font-style: normal;
                        </outputRendition>
                        <outputRendition scope="before">content: '[';</outputRendition>
                        <outputRendition scope="after">content: ']';</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="pc">
                     <model predicate="not(@join = 'right' or @join = 'both') and following-sibling::*[1] and not(parent::q/pc[last()] = node()) and not(parent::quote/pc[last()] = node())" output="web" behaviour="webcomponent">
                        <param name="content">node()</param>
                        <param name="space-after">true()</param>
                    </model>
                    <model output="web" behaviour="webcomponent">
                        <param name="content">node()</param>
                    </model>
                    <model output="epub" behaviour="inline">
                        <param name="content">node()</param>
                    </model>
                    <model behaviour="text"/>
                </elementSpec>
                <elementSpec mode="change" ident="w">
                    <model output="web" predicate="not(@join = 'right' or @join = 'both') and not(following-sibling::*[1][self::w and (@join = 'left' or @join='both')]) and not(following-sibling::*[1][self::pc and not(@join='right')]) and not(parent::q/w[last()] = node()) and not(parent::quote/w[last()] = node())" behaviour="webcomponent-orig">
                        <param name="content">node()</param>
                        <param name="space-after">true()</param>
                    </model>
                    <model output="web" behaviour="webcomponent-orig">
                        <param name="content">node()</param>
                    </model>
                    <model output="epub" behaviour="inline">
                        <param name="content">node()</param>
                    </model>
                    <model behaviour="text"/>
                </elementSpec>
                <elementSpec mode="change" ident="orig">
                    <model output="web" behaviour="webcomponent">
                        <param name="content">node()</param>
                    </model>
                    <model behaviour="text"/>
                </elementSpec>
                <elementSpec mode="change" ident="ref">
                    <model behaviour="inline" predicate="not(@target)"/>
                    <model predicate="not(text()) and ancestor::teiHeader" behaviour="link">
                        <param name="content">@target</param>
                        <param name="link">@target</param>
                    </model>
                    <model predicate="ancestor::teiHeader" behaviour="link">
                        <param name="link">@target</param>
                    </model>
                    <model predicate="text()" behaviour="inline">
                        <outputRendition>vertical-align: super; font-size: smaller;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="reg">
                    <model output="web" behaviour="webcomponent">
                        <param name="content">node()</param>
                    </model>
                    <model behaviour="text"/>
                </elementSpec>
                <elementSpec mode="change" ident="seg">
                        <model predicate="@type = 'speech_count' or @type = 'character_name'" behaviour="inline">
                            <outputRendition scope="before">content: ' ';</outputRendition>
                            <outputRendition scope="after">content: ' ';</outputRendition>
                        </model>
                        <model behaviour="inline" useSourceRendition="true"/>
                </elementSpec>
                <elementSpec ident="sic" mode="change">
                        <model predicate="parent::choice and count(parent::*/*) gt 1" behaviour="inline"/>
                        <model behaviour="inline">
                            <outputRendition scope="before">content: ' {';</outputRendition>
                            <outputRendition>font-weight:bold;</outputRendition>
                            <outputRendition scope="after">content: '} ';</outputRendition>
                        </model>
                </elementSpec>
                <elementSpec mode="change" ident="div">
                    <model predicate="@rend = 'empty-page'" behaviour="block" cssClass="empty-page"/>
                    <model behaviour="block"/>
                </elementSpec>
                <elementSpec mode="add" ident="ep:epHeader">
                    <model behaviour="omit"/>
                </elementSpec>
                <elementSpec mode="change" ident="floatingText">
                    <model behaviour="block">
                        <outputRendition>margin: 14pt;</outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="sp">
                    <model behaviour="block">
                        <outputRendition> margin-top: 0.25em; margin-bottom: 0.25em </outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="speaker">
                    <model behaviour="inline">
                        <outputRendition> font-style:italic; margin-left: 0.5em </outputRendition>
                    </model>
                </elementSpec>
                <elementSpec mode="change" ident="l">
                    <model predicate="preceding-sibling::*[1][self::speaker]" behaviour="inline">
                        <outputRendition> margin-left: 0.5em; </outputRendition>
                    </model>
                    <model behaviour="block" useSourceRendition="true">
                        <outputRendition> margin-left: 1em; </outputRendition>
                    </model>
                 </elementSpec>
                <elementSpec mode="change" ident="p">
                    <model predicate="preceding-sibling::*[1][self::speaker]" behaviour="inline">
                       <outputRendition>text-align: justify; margin-left: 0.5em;</outputRendition>
                    </model>
                    <model behaviour="paragraph" useSourceRendition="true">
                        <outputRendition>text-align: justify;</outputRendition>
                    </model>
                </elementSpec>
                <moduleRef key="tei"/>
                <moduleRef key="header"/>
                <moduleRef key="tagdocs"/>
                <moduleRef key="core"/>
                <moduleRef key="gaiji"/>
                <moduleRef key="namesdates"/>
                <moduleRef key="msdescription"/>
                <moduleRef key="corpus"/>
                <moduleRef key="transcr"/>
                <moduleRef key="analysis"/>
                <moduleRef key="linking"/>
                <moduleRef key="drama"/>
                <moduleRef key="textstructure"/>
                <moduleRef key="figures"/>
                <moduleRef key="verse"/>
            </schemaSpec>
        </body>
    </text>
</TEI>