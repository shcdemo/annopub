xquery version "3.0";

import module namespace registration-config="http://existsolutions.com/registration-config" at "self-registration-config.xqm";

declare option exist:serialize "method=html5 media-type=text/html";
    let $token := request:get-parameter('token','1112')
    let $registration := collection($registration-config:REGISTRATION-PATH)//registration-request[@token eq $token]
    let $user := $registration/@user
    let $pass := $registration/@pass

    return
      if (not($registration)) then (
        (:response:set-status-code(404):)
          doc($registration-config:APP-PATH || "/components/existdb-register/no-registration.html")
      )
      else (
          if (sm:user-exists($user)) then (
              (: response:set-status-code(500) :)
              doc($registration-config:APP-PATH || "/components/existdb-register/user-exists.html")
          ) else (
              (: check if request has not expired :)
              if (xs:dateTime($registration/@expires) < xs:dateTime(current-date())) then (
                (: delete registration request :)
                let $delete := update delete $registration
                return doc($registration-config:APP-PATH || "/components/existdb-register/expired.html")
              )
              else (
                  (: create user account for app :)
                  let $create := sm:create-account($user, $pass, $registration-config:USER-GROUP, "")
                  (: delete registration request :)
                  (:let $delete := update delete $registration:)
                  (: send back success message :)
                  return doc($registration-config:APP-PATH || "/components/existdb-register/confirmation.html")
              )
          )
      )
