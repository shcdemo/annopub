(:
 : Copyright 2015, Wolfgang Meier
 :
 : This software is dual-licensed:
 :
 : 1. Distributed under a Creative Commons Attribution-ShareAlike 3.0 Unported License
 : http://creativecommons.org/licenses/by-sa/3.0/
 :
 : 2. http://www.opensource.org/licenses/BSD-2-Clause
 :
 : All rights reserved. Redistribution and use in source and binary forms, with or without
 : modification, are permitted provided that the following conditions are met:
 :
 : * Redistributions of source code must retain the above copyright notice, this list of
 : conditions and the following disclaimer.
 : * Redistributions in binary form must reproduce the above copyright
 : notice, this list of conditions and the following disclaimer in the documentation
 : and/or other materials provided with the distribution.
 :
 : This software is provided by the copyright holders and contributors "as is" and any
 : express or implied warranties, including, but not limited to, the implied warranties
 : of merchantability and fitness for a particular purpose are disclaimed. In no event
 : shall the copyright holder or contributors be liable for any direct, indirect,
 : incidental, special, exemplary, or consequential damages (including, but not limited to,
 : procurement of substitute goods or services; loss of use, data, or profits; or business
 : interruption) however caused and on any theory of liability, whether in contract,
 : strict liability, or tort (including negligence or otherwise) arising in any way out
 : of the use of this software, even if advised of the possibility of such damage.
 :)
xquery version "3.1";

(:~
 : Template functions to handle page by page navigation and display
 : pages using TEI Simple.
 :)
module namespace pages="http://www.tei-c.org/tei-simple/pages";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace expath="http://expath.org/ns/pkg";
declare namespace ep="http://earlyprint.org/ns/1.0";

import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace app="http://www.tei-c.org/tei-simple/templates" at "app.xqm";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace odd="http://www.tei-c.org/tei-simple/odd2odd" at "../../tei-simple/content/odd2odd.xql";
import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util" at "../../tei-simple/content/util.xql";
import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "../../annotation-service/modules/annotation.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx = "http://www.functx.com";

declare variable $pages:app-root := request:get-context-path() || substring-after($config:app-root, "/db");

declare
    %templates:wrap
function pages:load($node as node(), $model as map(*), $doc as xs:string, $root as xs:string?,
                    $page as xs:string?, $view as xs:string?) {
    let $doc := xmldb:encode-uri($doc)
    let $view := if ($view) then $view else $config:default-view
    let $root :=
        if ($page) then
            let $x := console:log("Loading by page " || $page)
            let $docname := $config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc

            (:  xml:id on pb element may be of the form A16527_03-090-b or A16527-090-b for split texts :)
            let $pb := doc($docname)//id(functx:substring-before-last($doc, '.') || '-' || $page)
            let $pb :=  if ($pb)
                        then $pb
                        else doc($docname)//id(functx:substring-before-last($doc, '_') || '-' || $page)
            let $nodeid := util:node-id($pb)
            return $nodeid
        else $root
    let $data := pages:load-xml($view, $root, $doc)

    return
        map {
            "data": $data,
            "document-id": root($data)/tei:TEI/@xml:id,
            "version": root($data)/tei:TEI/@n
        }
};

declare function pages:load-xml($view as xs:string?, $root as xs:string?, $doc as xs:string) {
    let $view := if ($view) then $view else $config:default-view
    return
        switch ($view)
    	    case "div" return
        	    if (matches($doc, "_\d+\.[\d\.]+\.xml$")) then
                    let $analyzed := analyze-string($doc, "^(.*)_(\d+\.[\d\.]+)\.xml$")
                    let $docName := $analyzed//fn:group[@nr = 1]/text()
                    return
                        util:node-by-id(doc($config:data-root || "/" || substring($docName, 1, 3) || "/" || $docName), $analyzed//fn:group[@nr = 2]/string())
                else if ($root) then
                        util:node-by-id(doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc), $root)
                else
                    let $div := (doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)//tei:div)[1]
                    return
                        if ($div) then
                            $div
                        else
                            doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)/tei:TEI//tei:body
            case "page" return
                if (matches($doc, "_\d+\.[\d\.]+\.xml$")) then
                    let $analyzed := analyze-string($doc, "^(.*)_(\d+\.[\d\.]+)\.xml$")
                    let $docName := $analyzed//fn:group[@nr = 1]/text()
                    let $targetNode := util:node-by-id(doc($config:data-root || "/" || substring($docName, 1, 3) || "/" || $docName), $analyzed//fn:group[@nr = 2]/string())
                    return
                        $targetNode
                else if ($root) then
                    util:node-by-id(doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc), $root)
                else
                    let $pb := (doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)//tei:pb[not(@type) or @type != 'blank'])[1]
                    return
                        if ($pb) then
                            $pb
                        else
                            doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)/tei:TEI//tei:body
            default return
                doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)/tei:TEI/tei:text
};

declare function pages:get-annotation-filters-for($user as map(*)) as xs:string* {
    switch ($user?role)
        case 'shcadmin' return (
                annotation:get-filter-string('earlyPrint', (), 'public', 'pending'),
                annotation:get-filter-string('earlyPrint', (), 'public', 'accepted'),
                annotation:get-filter-string('earlyPrint', $user?name, (), ())
            )
        case 'shcuser' return (
                annotation:get-filter-string('earlyPrint', (), 'public', 'pending'),
                annotation:get-filter-string('earlyPrint', (), 'public', 'accepted'),
                annotation:get-filter-string('earlyPrint', $user?name, (), ())
            )
        default return (
                annotation:get-filter-string('earlyPrint', (), 'public', 'accepted'),
                annotation:get-filter-string('earlyPrint', (), 'public', 'pending')
            )
};

declare
    %templates:wrap
function pages:annotation-list($node as node(), $model as map(*)) {
    let $user := app:current-user-and-role()
    return annotation:get-annotations(
        $model?document-id/string(),
        $model?version/string(),
        $model?xml,
        pages:get-annotation-filters-for($user)
    )
};


declare function pages:back-link($node as node(), $model as map(*)) {
    element { node-name($node) } {
        attribute href {
            $pages:app-root || "/works/"
        },
        $node/@*,
        $node/node()
    }
};

declare function pages:single-page-link($node as node(), $model as map(*), $doc as xs:string) {
    element { node-name($node) } {
        $node/@* except $node/@href,
        attribute href { "?view=plain" },
        $node/node()
    }
};

declare function pages:add-document-metadata($node as node(), $model as map(*), $doc as xs:string) {
    element { node-name($node) } {
        $node/@*,
        attribute id { $model?document-id },
        attribute source { $model?document-id },
        attribute version { $model?version },
        templates:process($node/*, $model)
    }
};

declare function pages:xml-link($node as node(), $model as map(*), $source as xs:string?) {
    let $doc-path :=
        if ($source = "odd") then
            $config:odd-root || "/" || $config:odd
        else if ($model?work) then
            document-uri(root($model?work))
        else if ($model?data) then
            document-uri(root($model?data))
        else
            $config:app-root || "/" || $source
    let $rest-link := '/exist/rest' || $doc-path
    return
        element { node-name($node) } {
            $node/@* except ($node/@href, $node/@class),
            attribute href { $rest-link },
            attribute target { "_blank" },
            $node/node()
        }
};

declare
    %templates:wrap
    %templates:default("action", "browse")
    %templates:default("lucene-query-mode", "all")
function pages:load-view($node as node(), $model as map(*), $view as xs:string?, $action as xs:string, $lucene-query-mode as xs:string) {
    let $view := if ($view) then $view else $config:default-view
    let $data := $model?data
    let $xml :=
        if ($view = ("div", "page")) then
            pages:get-content(root($model?data), $data[1])
        else
            $model?data//*:body/*
    let $data :=
        if ($xml/tei:pb and count($xml/*) = 1) then
            element { node-name($xml) } {
                $xml/@* except $xml/@rend,
                attribute rend { "empty-page" },
                $xml/node()
            }
        else
            $xml
    return
        map {
            "xml": $data
        }
};


declare
    %templates:default("action", "browse")
function pages:view($node as node(), $model as map(*), $view as xs:string?, $action as xs:string) {
        pages:process-content($model?xml, $model?data)
};

declare function pages:process-content($xml as node()*, $root as node()*) {
    pages:process-content($xml, $root, (), ())
};

declare function pages:process-content($xml as node()*, $root as node()*, $config as map(*)?, $userParams as map(*)?) {
    let $params := map:merge((
            map {
                "root": $root,
                "view": $config?view
            },
            $userParams))
	let $html := $pm-config:web-transform($xml, $params, $config?odd)
    let $class := if ($html//*[@class = ('margin-note')]) then "margin-right" else ()
    return
        <div class="content {$class}">
        {$html}
        </div>
};

declare %private function pages:binary-search($depth as xs:integer, $target, $list,
                                              $start as xs:integer, $end as xs:integer) {

    let $middleIndex := xs:integer(  xs:integer(($end - $start) div 2) + xs:integer($start))
    let $middleElement := $list[$middleIndex]
    return
        if ($depth < 15 and $middleElement >> $target) then
            pages:binary-search($depth + 1, $target, $list, $start, $middleIndex)
        else if ($depth < 15 and $middleElement << $target) then
            pages:binary-search($depth + 1, $target, $list, $middleIndex, $end)
        else
            $middleElement
};

declare
    %templates:wrap
function pages:table-of-contents($node as node(), $model as map(*), $view as xs:string?) {
    let $start-time := util:system-time()
    let $view := if ($view) then $view else $config:default-view

    let $pblist :=
        if ($view = "page") then
            for $pb in root($model?data)//tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')]
            return $pb
        else ()

    let $return :=  pages:toc-div(root($model?data), $view, $pblist)
    let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
    let $x := console:log("TOC " || $view || " view generated in " || $seconds || "s for " || xs:string(count($pblist)) || " pages.")
    return $return
};

declare %private function pages:toc-div($node, $view as xs:string?, $pblist) {
    let $view := if ($view) then $view else $config:default-view
    let $divs := $node//tei:div[tei:head] except $node//tei:div[tei:head]//tei:div
(:    let $divs := $node//tei:div[empty(ancestor::tei:div) or ancestor::tei:div[1] is $node][tei:head]:)
    return
        if (empty($divs)) then
            ()
        else
        <ul>
        {
            for $div in $divs
            let $html := for-each($div/tei:head, function($head) {
                if ($head/(ancestor::tei:note|ancestor::tei:reg|ancestor::tei:sic)) then
                    ()
                else
                    fn:replace( fn:replace( fn:normalize-space(xs:string($head)), "\s+([,\.!?:;\)\]]+)", "$1" ), "\(\s+", "(" )

            })
            let $root := (
                if ($view = "page") then
                    (: exception for back matter divs which do not contain any pb
                    for display of machine generated castlists and textual notes                   :)
                    if($div/ancestor::tei:back and not($div//tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')])) then
                        $div 
                    else
                        pages:binary-search(0, $div, $pblist, 1, count($pblist))
                else
                    (),
                $div
            )[1]
            let $nav_component := if ($view eq "page")
                                  then "?page=" || substring-after($root/@xml:id, '-')
                                  else "?root=" || util:node-id($root) || "&amp;view=div"
            return
                <li>
                    <a class="toc-link" data-bs-dismiss="offcanvas" href="{util:document-name($div)}{$nav_component}">{$html}</a>
                    {pages:toc-div($div, $view, $pblist)}
                </li>
        }
        </ul>
};

declare
    %templates:wrap
function pages:styles($node as node(), $model as map(*)) {
    attribute href {
        let $name := replace($config:odd, "^([^/\.]+).*$", "$1")
        return
            $pages:app-root || "/" || $config:output || "/" || $name || ".css"
    }
};

declare
    %templates:wrap
function pages:navigation($node as node(), $model as map(*), $view as xs:string?) {
    let $view := if ($view) then $view else $config:default-view
    let $div := $model?data
    let $work := $div/ancestor-or-self::tei:TEI
    return
        switch ($view)
            case "single" return
                map {
                    "div" : $div,
                    "work" : $work
                }
            case "page" return
                map {
                    "previous": $div/preceding::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1],
                    "next": $div/following::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1],
                    "work": $work,
                    "div": $div
                }
            default return
                let $parent := $div/ancestor::tei:div[not(*[1] instance of element(tei:div))][1]
                let $prevDiv := $div/preceding::tei:div[1]
                let $prevDiv := pages:get-previous(if ($parent and (empty($prevDiv) or $div/.. >> $prevDiv)) then $div/.. else $prevDiv)
                let $nextDiv := pages:get-next($div)
            (:        ($div//tei:div[not(*[1] instance of element(tei:div))] | $div/following::tei:div)[1]:)
                return
                    map {
                        "previous" : $prevDiv,
                        "next" : $nextDiv,
                        "work" : $work,
                        "div" : $div
                    }
};

declare function pages:get-next($div as element()) {
    if ($div/tei:div) then
        if (count(($div/tei:div[1])/preceding-sibling::*) < 5)  then
            pages:get-next($div/tei:div[1])
        else
            $div/tei:div[1]
    else
        $div/following::tei:div[1]
};

declare function pages:get-previous($div as element(tei:div)?) {
    if (empty($div)) then
        ()
    else
        if (
            empty($div/preceding-sibling::tei:div)  (: first div in section :)
            and count($div/preceding-sibling::*) < 5 (: less than 5 elements before div :)
            and $div/.. instance of element(tei:div) (: parent is a div :)
        ) then
            pages:get-previous($div/..)
        else
            $div
};

declare function pages:get-content($root as document-node(), $node as element()) {
    typeswitch ($node)
        case element(tei:teiHeader) return
            $node
        case element(tei:pb) return (
            let $nextPage := $node/following::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1]
            let $start-time := util:system-time()
            let $chunk := pages:milestone-chunk($node,
                                                $nextPage,
                                                if ($nextPage) then
                                                    ($node/ancestor::* intersect $nextPage/ancestor::*)[last()]
                                                else
                                                    ($node/ancestor::tei:div, $node/ancestor::tei:body)[1]
            )
            return
                let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
                let $x := console:log("Fetched page " || $node/@xml:id || " in " || $seconds || "s.")
                return $chunk
        )
        case element(tei:div) return
            if ($node/tei:div) then
                if (count(($node/tei:div[1])/preceding-sibling::*) < 5) then
                    let $child := $node/tei:div[1]
                    return
                        element { node-name($node) } {
                            $node/@*,
                            attribute exist:id { util:node-id($node) },
                            util:expand(($child/preceding-sibling::*, $child), "add-exist-id=all")
                        }
                else
                    element { node-name($node) } {
                        $node/@*,
                        attribute exist:id { util:node-id($node) },
                        util:expand($node/tei:div[1]/preceding-sibling::*, "add-exist-id=all")
                    }
            else
                util:expand($node)
        default return
            $node
};

declare %private function pages:milestone-chunk($ms1 as element(), $ms2 as element()?, $node as node()*) as node()*
{
    typeswitch ($node)
        case element() return
            if ($node is $ms1) then
                util:expand($node, "add-exist-id=all")
            else if ( some $n in $node/descendant::* satisfies ($n is $ms1 or $n is $ms2) ) then
                element { node-name($node) } {
                    $node/@*,
                    for $i in ( $node/node() )
                    return pages:milestone-chunk($ms1, $ms2, $i)
                }
            else if ($node >> $ms1 and (empty($ms2) or $node << $ms2)) then
                util:expand($node, "add-exist-id=all")
            else
                ()
        case attribute() return
            $node (: will never match attributes outside non-returned elements :)
        default return
            if ($node >> $ms1 and (empty($ms2) or $node << $ms2)) then $node
            else ()
};

declare
    %templates:wrap
function pages:navigation-title($node as node(), $model as map(*)) {
    pages:title($model('data')/ancestor-or-self::tei:TEI)
};

declare function pages:title($work as element()) {
   let $main-title := $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'short']/text()
   let $main-title :=
       if ($main-title) then $main-title else
           $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main']/text()
   let $main-title :=
       if ($main-title) then $main-title else $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]/text()
   let $main-title := normalize-space( $main-title )
   return
       if ( string-length( $main-title) > 70 ) then
           concat( substring( $main-title, 0, 70 ), "..." )
       else
           $main-title
};


declare
    %templates:wrap
function pages:header-witness-note($node as node(), $model as map(*)) {
    pages:facsimile-witness-note($model('data')/ancestor-or-self::tei:TEI)
};

declare function pages:facsimile-witness-note($work as element()) {
    let $witness-note := $work/tei:facsimile/tei:surfaceGrp/tei:note[@type = 'witnessDetail']/text()
    return
        if ($witness-note) then $witness-note else ''
};

declare
    %templates:replace
function pages:app-root($node as node(), $model as map(*)) {
    element { node-name($node) } {
        $node/@*,
        attribute data-app { request:get-context-path() || substring-after($config:app-root, "/db") },
        templates:process($node/*, $model)
    }
};

declare function pages:switch-view($node as node(), $model as map(*), $root as xs:string?, $doc as xs:string, $view as xs:string?) {
    let $view := if ($view) then $view else $config:default-view
    let $targetView := if ($view = "page") then "div" else "page"
    let $root := pages:switch-view-id($model?data, $view)
    let $viewparam := if ($targetView eq $config:default-view)
                      then ""
                      else "&amp;view=" || $targetView
    return
        element { node-name($node) } {
            $node/@* except $node/@class,
            if (pages:has-pages($model?data) and $root) then (
                if ($targetView eq "page") then
                    attribute href {
                        "?page=" || $root/@xml:id || $viewparam
                    }
                else
                    attribute href {
                        "?root=" || util:node-id($root) || $viewparam
                    },
                if ($view = "page") then (
                    attribute aria-pressed { "true" },
                    attribute class { $node/@class || " active" }
                ) else
                    $node/@class
            ) else (
                $node/@class,
                attribute disabled { "disabled" }
            ),
            templates:process($node/node(), $model)
        }
};

declare function pages:has-pages($data as element()+) {
    exists((root($data)//tei:div)[1]//tei:pb)
};

declare function pages:switch-view-id($data as element()+, $view as xs:string) {
    let $root :=
        if ($view = "div") then
            ($data/*[1][self::tei:pb], $data/preceding::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1])[1]
        else
            $data/ancestor::tei:div[1]
    return
        $root
};
