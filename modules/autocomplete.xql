xquery version "3.1";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";
declare namespace json = "http://www.json.org";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

declare option output:method "json";
declare option output:media-type "application/json";

let $q := request:get-parameter("q", ())
let $type := request:get-parameter("type", "title")
let $items :=
    if ($q) then
        switch ($type)
            case "author" return
                let $authors := distinct-values(ft:search($config:data-root, "author:" || $q || "*", "author")//field)
                return
                    for $a in $authors
                    where fn:contains(fn:lower-case($a), fn:lower-case($q))
                    return $a
            case "title" return
                distinct-values(ft:search($config:data-root, "title:" || $q || "*", "title")//field)
            case "curator" return
                let $curators := distinct-values(ft:search($config:data-root, "curator:" || $q || "*", "curator")//field)
                return
                    for $c in $curators
                    where fn:contains(fn:lower-case($c), fn:lower-case($q))
                    return $c
            case "date"
            case "date2" return
                distinct-values(ft:search($config:data-root, "date:" || $q || "*", "date")//field)
            case "identifier" return
                distinct-values(ft:search($config:data-root, "identifier:" || $q || "*", "identifier")//field)
             case "keyword" return
                let $keywords := distinct-values(ft:search($config:data-root, "keyword:" || $q || "*", "keyword")//field)
                return
                    for $k in $keywords
                    where fn:contains(fn:lower-case($k), fn:lower-case($q))
                    return $k
            case "proofreader" return
                let $proofreaders := distinct-values(ft:search($config:data-root, "proofreader:" || $q || "*", "proofreader")//field)
                return
                    for $p in $proofreaders
                    where fn:contains(fn:lower-case($p), fn:lower-case($q))
                    return $p
            case "creator" return
                let $creators := distinct-values(('autocorrect', sm:get-group-members("shcuser")))
                return
                    for $c in $creators
                    where fn:contains(fn:lower-case($c), fn:lower-case($q))
                    return map {
                        "value": $c,
                        "label": $c
                    }
            case "modifier" return
                let $modifiers := sm:get-group-members("shcuser")
                return
                    for $m in $modifiers
                    where fn:contains(fn:lower-case($m), fn:lower-case($q))
                    return map {
                        "value": $m,
                        "label": $m
                    }
            default return
                distinct-values(ft:search($config:data-root, "title:" || $q || "*", "title")//field)
    else
        ()
return
    array { $items }
