xquery version "3.0";

import module namespace registration="http://existsolutions.com/registration" at "self-registration.xqm";
import module namespace xqmail="http://existsolutions.com/mailmodule" at "../xqmail/xqmail.xqm";
import module namespace registration-config="http://existsolutions.com/registration-config" at "self-registration-config.xqm";


let $user := request:get-parameter('user','joern@existsolutions.com')
let $pass := request:get-parameter('password', '12345678')



let $token := registration:register($user, $pass)


let $url := "http://" || request:get-server-name() || ":" || request:get-server-port() || "/exist/apps/" || $registration-config:APP || "/components/existdb-register/process-confirmation.xql"

(: not a nice place for changing the owner but where else? :)
let $foo := sm:chmod(xs:anyURI("/db/apps/shc/components/existdb-register/self-registration.xqm"), "rwsr-xr-x")

(: the email subject for the confirmation mail :)

 
let $subject := "Your registration to the EarlyModernLab"
let $bodytext := "Thanks for your registration to the EarlyModernLab. &#10;&#10; To activate your account please click the following link: &#10;&#10;http://localhost:8080/exist/db/apps/shc/modules/registration/process-confirmation.xql?token=" || $token
let $html :=
    <div>
        <h1>Welcome to the Early Modern Lab</h1>
        <p>To activate your account please click the following link:</p>
        <a href="{$url}?token={$token}">Activate your account now</a>
    </div>


return xqmail:sendMail($user,(),(),$subject,$bodytext,$html, ())

return $user
