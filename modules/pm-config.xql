xquery version "3.0";

module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util";

declare variable $pm-config:web-transform := pm-config:process(?, ?, ?, "web");

declare function pm-config:process($xml as node()*, $parameters as map(*)?, $odd as xs:string?, $outputMode as xs:string) {
    let $oddName := ($odd, $config:odd)[1]
    return
        pmu:process($config:odd-root || "/" || $oddName, $xml, $config:output-root, $outputMode,
            "../" || $config:output, $config:module-config, $parameters)
};
