xquery version "3.0";

import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util";
import module namespace odd="http://www.tei-c.org/tei-simple/odd2odd";

declare namespace repo="http://exist-db.org/xquery/repo";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

declare variable $repoxml :=
    let $uri := doc($target || "/expath-pkg.xml")/*/@name
    let $repo := util:binary-to-string(repo:get-resource($uri, "repo.xml"))
    return
        parse-xml($repo)
;

declare function local:generate-code($collection as xs:string) {
    for $source in xmldb:get-child-resources($collection || "/resources/odd")[ends-with(., ".odd")]
    for $module in ("web")
    for $file in pmu:process-odd(
        odd:get-compiled($collection || "/resources/odd", $source),
        $collection || "/transform",
        $module,
        "../transform",
        doc($collection || "/resources/odd/configuration.xml")/*)?("module")
    return
        ()
};

declare function local:set-generated-permissions($collection as xs:string) {
    let $permissions := $repoxml//repo:permissions[1]
    return (
        for $file in xmldb:get-child-resources($collection || "/transform")
        let $path := xs:anyURI($collection || "/transform/" || $file)
        return (
            sm:chown($path, $permissions/@user),
            sm:chgrp($path, $permissions/@group)
        )
    )
};

sm:chmod(xs:anyURI($target || "/modules/view.xql"), "rwsr-xr-x"),
sm:chmod(xs:anyURI($target || "/modules/ajax.xql"), "rwsr-xr-x"),
sm:chmod(xs:anyURI($target || "/modules/lib/api/regenerate.xql"), "rwsr-xr-x"),

(:
  self-registration needs to set GID
  to write to registration collection
:)
sm:chgrp(xs:anyURI($target || "/components/existdb-register/shc-register.xql"), "dba"),
sm:chmod(xs:anyURI($target || "/components/existdb-register/shc-register.xql"), "rwxr-Sr-x"),

(:
  confirmation needs to set UID and GID
  to execute functions in securitymanager sm:*
:)
sm:chown(xs:anyURI($target || "/components/existdb-register/process-confirmation.xql"), "admin"),
sm:chgrp(xs:anyURI($target || "/components/existdb-register/process-confirmation.xql"), "dba"),
sm:chmod(xs:anyURI($target || "/components/existdb-register/process-confirmation.xql"), "rwSr-Sr-x"),

sm:group-exists("shcuser") or sm:create-group("shcuser"),

local:generate-code($target),
local:set-generated-permissions($target)
