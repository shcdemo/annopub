xquery version "3.0";

module namespace registration-config="http://existsolutions.com/registration-config";

(: configuration params - adapt to your needs :)

declare variable $registration-config:APP := "shc";
declare variable $registration-config:APP-PATH := "/db/apps/" || $registration-config:APP;
declare variable $registration-config:ROOT-COL := $registration-config:APP-PATH || "/data";

declare variable $registration-config:REGISTRATION-COL := "registration";
declare variable $registration-config:REGISTRATION-PATH := $registration-config:ROOT-COL || "/" || $registration-config:REGISTRATION-COL;
declare variable $registration-config:USER-GROUP := "shcuser";

