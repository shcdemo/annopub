xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

declare option output:method "json";
declare option output:media-type "application/json";

let $doc :=  request:get-parameter("doc", ())
let $text := $config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc

let $result :=  array {
    for $pb in doc($text)//tei:pb[not(@type) or @type != 'blank']
    return
        map {
            "pbid": data($pb/@xml:id),
            "nodeid": util:node-id($pb)
        }
}
return map { "pages": $result }


