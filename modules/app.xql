xquery version "3.1";

module namespace app="http://www.tei-c.org/tei-simple/templates";

import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace pages="http://www.tei-c.org/tei-simple/pages" at "pages.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx = "http://www.functx.com";
import module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline" at "../../annotation-service/modules/standoff2inline.xqm";
import module namespace memsort="http://exist-db.org/xquery/memsort" at "java:org.existdb.memsort.SortModule";

declare namespace expath="http://expath.org/ns/pkg";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";

declare variable $app:max-hits := 10000;

declare
    %templates:wrap
function app:check-login($node as node(), $model as map(*)) {
    let $user := request:get-attribute($config:login-domain || ".user")
    return
        if ($user) then
            templates:process($node/*[2], $model)
        else
            templates:process($node/*[1], $model)
};

declare
    %templates:wrap
function app:current-user($node as node(), $model as map(*)) {
    request:get-attribute($config:login-domain || ".user")
};

declare function app:current-user-and-role() as map(*) {
  let $loginname := request:get-attribute($config:login-domain || ".user")
  let $userinfo := sm:id()//sm:real[./sm:username/string() eq $loginname]
  let $name := if ($userinfo) then ($userinfo/sm:username/string()) else ('guest')
  let $role := ($userinfo/sm:groups/sm:group[. = 'shcadmin']/string(),
                $userinfo/sm:groups/sm:group[. = 'shcuser']/string(),
                'reader')[1]

  return map {
    'name': $name,
     'role': $role
  }
};

declare
    %templates:replace
function app:get-user-meta($node as node()*, $model as map(*)) as element()+ {
    let $shcinfo := app:current-user-and-role()
    return (
        <script type="application/javascript">
            sessionStorage.setItem("user", "{$shcinfo?name}");
            sessionStorage.setItem("role", "{$shcinfo?role}");
        </script>
    )
};

declare
    %templates:wrap
function app:show-if-logged-in($node as node(), $model as map(*)) {
    let $user := request:get-attribute($config:login-domain || ".user")
    return
        if ($user) then
            templates:process($node/node(), $model)
        else
            ()
};

(: Nasty workaround because eXist's Lucene implementation before 5.x does not support ranges.
 : What we want is
 :  date:[1601 TO 1605]
 : but what we have to do is
 :  date:(1601 OR 1602 OR 1603 OR 1604 OR 1605)
 :)

declare
function local:date-list($date as xs:integer, $date2 as xs:integer) as xs:string {
    let $years :=
        if ($date2 ge $date
            and $date ge 1475
            and $date le 1850
            and $date2 ge 1475
            and $date2 le 1850)
        then
            string-join(
                for $year in ($date to $date2)
                return $year, " OR "
            )
        else ()

    return "(" || $years || ")"
};

(: Nasty workaround because eXist's Lucene implementation before 5.x does not support ranges.
 : What we want is grade:[B TO F] but what we have to do is grade:(B OR C OR D OR F).
 :)
declare
function local:grade-list($grade as xs:string, $gradeop as xs:string) as xs:string {

    let $grades :=
            string-join(
                for $g in ("A", "B", "C", "D", "F")
                    return
                    switch ($gradeop)
                        case "ge" return if ($g ge $grade) then $g else ()
                        case "le" return if ($g le $grade) then $g else ()
                        default return if ($g eq $grade) then $g else ()
                ,
                " OR "
            )

    return "(" || $grades || ")"
};

(:~
 : Build main work filter.
:)

declare %private function app:build-work-filter
(
    $author, $title, $date, $date2, $genre, $keyword,
    $identifier, $proofreader, $curator, $grade, $gradeop,
    $haspageimages, $corpus
)
{
    let $date := replace($date, "[^\d]", "")
    let $date2 := replace($date2, "[^\d]", "")

    let $querylist :=
    (
        if (string-length($author)) then "author:" || $author else (),
        if (string-length($title)) then "title:" || $title else (),
        if (string-length($date))
        then "date:"  ||
                (if (string-length($date2)) then local:date-list($date, $date2) else $date)
        else (),
        if (string-length($genre)) then "genre:" || $genre else (),
        if (string-length($keyword)) then "keyword:" || $keyword else (),
        if (string-length($identifier)) then "identifier:" || $identifier else (),
        if (string-length($proofreader)) then "proofreader:" || $proofreader else (),
        if (string-length($curator)) then "curator:" || $curator else (),
        if (string-length($grade))
        then "grade:" ||
            (if (string-length($gradeop)) then local:grade-list($grade, $gradeop) else $grade)
        else (),
        if (string-length($haspageimages)) then "haspageimages:" || $haspageimages else (),
        if (string-length($corpus)) then "corpus:" || $corpus else ()
    )

    return string-join($querylist, ' AND ')
};

(:~
 : Extended work filter with page and word counts if necessary.
:)

declare %private function app:build-extended-work-filter
(
    $filter,
    $pagecount,
    $pagecount2,
    $wordcount,
    $wordcount2
)
{
    let $pagefilter := ""
    let $wordfilter := ""

    let $p1 :=
        if ( string-length( $pagecount ) ) then
            fn:number( $pagecount )
        else
            1

    let $p2 :=
        if ( string-length( $pagecount2 ) ) then
            fn:number( $pagecount2 )
        else
            $p1

    let $p2 :=
        if ( $p2 < $p1 ) then
            $p1
        else
            $p2

    let $pagefilter :=
        if ( string-length( $pagecount ) ) then
            if ( empty( $filter ) or ( $filter = "" ) ) then
                "pagecount:[" || $p1 || " TO " || $p2 || "]"
            else
                " AND pagecount:[" || $p1 || " TO " || $p2 || "]"
        else ()

    let $w1 :=
        if ( string-length( $wordcount ) ) then
            fn:number( $wordcount )
        else
            1

    let $w2 :=
        if ( string-length( $wordcount2 ) ) then
            fn:number( $wordcount2 )
        else
            $w1

    let $w2 :=
        if ( $w2 < $w1 ) then
            $w1
        else
            $w2

    let $wordfilter :=
        if ( string-length( $wordcount ) ) then
            if ( ( empty( $filter ) or ( $filter = "" ) ) and
                 ( empty( $pagefilter ) or ( $pagefilter = "" ) )
               )
            then
                "wordcount:[" || $w1 || " TO " || $w2 || "]"
            else
                " AND wordcount:[" || $w1 || " TO " || $w2 || "]"
        else ()

    return $filter || $pagefilter || $wordfilter
};

(:~
 : Apply page count filter.
 :)

declare %private function app:apply-pagecount-filter
(
    $documents as node()*,
    $pagecount,
    $pagecount2
)
{
    let $p1 :=
        if ( string-length( $pagecount ) ) then
            fn:number( $pagecount )
        else
            1

    let $p2 :=
        if ( string-length( $pagecount2 ) ) then
            fn:number( $pagecount2 )
        else
            $p1

    let $p2 :=
        if ( $p2 < $p1 ) then
            $p1
        else
            $p2

    let $filteredDocuments :=
        for $doc in $documents
            let $pc := app:work-pagecount( $doc )
            return
                if ( ( $pc >= $p1 ) and ( $pc <= $p2 ) )
                then
                    $doc
                else
                    ()

    return $filteredDocuments
};

(:~
 : Apply word count filter.
 :)

declare %private function app:apply-wordcount-filter
(
    $documents as node()*,
    $wordcount,
    $wordcount2
)
{
    let $w1 :=
        if ( string-length( $wordcount ) ) then
            fn:number( $wordcount )
        else
            1

    let $w2 :=
        if ( string-length( $wordcount2 ) ) then
            fn:number( $wordcount2 )
        else
            $w1

    let $w2 :=
        if ( $w2 < $w1 ) then
            $w1
        else
            $w2

    let $filteredDocuments :=
        for $doc in $documents
            let $wc := app:work-wordcount( $doc )
            return
                if ( ( $wc >= $w1 ) and ( $wc <= $w2 ) )
                then
                    $doc
                else
                    ()

    return $filteredDocuments
};

(:~

 :)
declare function app:get-filtered-documents (
    $author,
    $title,
    $date,
    $date2,
    $genre,
    $keyword,
    $identifier,
    $proofreader,
    $curator,
    $grade,
    $gradeop,
    $haspageimages,
    $pagecount,
    $pagecount2,
    $wordcount,
    $wordcount2,
    $corpus,
    $sortField,
    $sortDirection,
    $allIfNone as xs:boolean (: when true, get all documents if none specified in filter :)
    ) as node()* {

    let $start-time := util:system-time()

    (: Build main work filter. :)

    let $filter :=
        app:build-work-filter
        ( $author, $title, $date, $date2, $genre, $keyword,
          $identifier, $proofreader, $curator, $grade, $gradeop,
          $haspageimages, $corpus
        )

    (: Add inoperative range queries for word and page counts. :)
    (: If these ever work correctly, this would be the actual  :)
    (: filter subexpression to use.  For now we just need this :)
    (: to remember whether page and/or word ranges were part   :)
    (: of the filter specification.                            :)

    let $extendedfilter :=
        app:build-extended-work-filter
        (
            $filter,
            $pagecount,
            $pagecount2,
            $wordcount,
            $wordcount2
        )

    (: Get list of cached works, if any. :)

    let $cached := session:get-attribute("simple.works")

    (: Apply main filter to documents. :)

    let $filteredDocuments :=
        if ( $filter ) then
            let $selected :=
                for $item in ft:search($config:data-root, $filter)/search
                    return $item
                for $doc in $selected
                    return doc( $doc/@uri )/tei:TEI
        else if ( $cached and ( $filter != "" ) ) then
            $cached
        else
            if ($allIfNone) then collection($config:data-root)/tei:TEI else ()

    let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
    let $x := console:log("Found " || count($filteredDocuments) || " documents in " || $seconds || " seconds.")

    (: Apply page count and word count filters, if specified, to documents. :)

    let $pagecount := replace($pagecount, "[^\d]", "")
    let $pagecount2 := replace($pagecount2, "[^\d]", "")

    let $wordcount := replace($wordcount, "[^\d]", "")
    let $wordcount2 := replace($wordcount2, "[^\d]", "")

    let $filteredDocuments :=
        if ( string-length( $pagecount ) and exists( $filteredDocuments ) ) then
            app:apply-pagecount-filter
            (
                $filteredDocuments,
                $pagecount,
                $pagecount2
            )
        else
            $filteredDocuments

    let $filteredDocuments :=
        if ( string-length( $wordcount ) and exists( $filteredDocuments ) ) then
            app:apply-wordcount-filter
            (
                $filteredDocuments,
                $wordcount,
                $wordcount2
            )
        else
            $filteredDocuments

    (: Sort the filtered works. :)

    let $filteredDocuments :=
        if ( exists( $filteredDocuments ) ) then
            app:sort-documents( $filteredDocuments, $sortField , $sortDirection )
        else
            $filteredDocuments

    let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
    let $x := console:log("Documents sorted at " || $seconds || " seconds.")

    return  (
                session:set-attribute("simple.works", $filteredDocuments),
                session:set-attribute("author", $author),
                session:set-attribute("title", $title),
                session:set-attribute("date", $date),
                session:set-attribute("date2", $date2),
                session:set-attribute("genre", $genre),
                session:set-attribute("keyword", $keyword),
                session:set-attribute("identifier", $identifier),
                session:set-attribute("proofreader", $proofreader),
                session:set-attribute("curator", $curator),
                session:set-attribute("grade", $grade),
                session:set-attribute("gradeop", $gradeop),
                session:set-attribute("haspageimages", $haspageimages),
                session:set-attribute("pagecount", $pagecount),
                session:set-attribute("pagecount2", $pagecount2),
                session:set-attribute("wordcount", $wordcount),
                session:set-attribute("wordcount2", $wordcount2),
                session:set-attribute("corpus", $corpus),
                session:set-attribute("sortField", $sortField),
                session:set-attribute("sortDirection", $sortDirection),
                session:set-attribute("filter", $filter),
                session:set-attribute("extendedfilter", $extendedfilter),
                $filteredDocuments
            )
};


(:~
 : List documents in data collection
 :)

declare
    %templates:wrap
    %templates:default("sortField", "title")
    %templates:default("sortDirection", "asc")
function app:list-works
(
    $node as node(),
    $model as map(*),
    $author,
    $title,
    $date,
    $date2,
    $genre,
    $keyword,
    $identifier,
    $proofreader,
    $curator,
    $grade,
    $gradeop,
    $haspageimages,
    $pagecount,
    $pagecount2,
    $wordcount,
    $wordcount2,
    $corpus,
    $sortField,
    $sortDirection
) as map(*)
{
    (: Do the filtering in a separate function that can be reused by annotation review
     :)
    let $filteredDocuments :=
        app:get-filtered-documents(
            $author,
            $title,
            $date,
            $date2,
            $genre,
            $keyword,
            $identifier,
            $proofreader,
            $curator,
            $grade,
            $gradeop,
            $haspageimages,
            $pagecount,
            $pagecount2,
            $wordcount,
            $wordcount2,
            $corpus,
            $sortField,
            $sortDirection,
            true()
        )

    return
    (
        map
        {
            "all" : $filteredDocuments
        }
    )
};

(:~
 : Sort documents on a field.
 :)

declare
    %templates:wrap
    %templates:default("sortField", "title")
    %templates:default("sortDirection", "asc")
function app:sort-documents
(
    $docs as node()*,
    $sortField as xs:string?,
    $sortDirection as xs:string?
)
{
    (: Get index to retrieve field data for field on which to sort documents. :)

    let $index :=
        switch ($sortField)
            case "author" return
                "memsort-author"
            case "date" return
                "memsort-date"
            case "title" return
                "memsort-title"
            case "finalgrade" return
                "memsort-grade"
            case "pagecount" return
                "memsort-pagecount"
            case "wordcount" return
                "memsort-wordcount"
            default return
                "memsort-title"

    (: Use FLOWR to sort documents by chosen field,
     :  in either ascending or descending order.
     :)

    let $direction := if ($sortDirection eq "desc") then "descending" else "ascending"
    let $order_by := 'stable order by memsort:get("' || $index || '", $doc) ' || $direction
    let $query := "for $doc in $docs " || $order_by || " return $doc"
    (: If the memsort indexes have not been created, we'll just return everything in
     : random order, which seems better than crashing.
     :)
    let $result := try {
        util:eval($query)
    }
    catch * {
        for $doc in $docs return $doc
    }
    return $result
};

declare
    %templates:wrap
    %templates:default("start", 1)
    %templates:default("per-page", 10)
function app:browse($node as node(), $model as map(*), $start as xs:int, $per-page as xs:int, $extendedfilter as xs:string?) {
    if (empty($model?all) and (empty($extendedfilter) or $extendedfilter = "")) then
        templates:process($node/*[@class="empty"], $model)
    else
        subsequence($model?all, $start, $per-page) !
            templates:process($node/*[not(@class="empty")], map:merge(($model, map { "work": . })))
};


(:~
 : Build main annotation filter.
:)

declare function app:build-annotation-filter
(
    $creator, $modifier, $status, $visibility
)
{
    let $querylist :=
    (
        if (string-length($creator)) then '@creator="' || $creator || '"' else '@creator!="autocorrect"',
        if (string-length($modifier)) then '@modifier="' || $modifier || '"' else (),
        if (string-length($status)) then '@status="' || $status || '"' else (),
        if (string-length($visibility)) then '@visibility="' || $visibility || '"' else '@visibility="public"'
    )

    return string-join($querylist, ' and ')
};


declare
    %templates:wrap
function app:list-annotations
(
    $node as node(),
    $model as map(*),
    $status,
    $creator,
    $modifier,
    $start,
    $author,
    $title,
    $date,
    $date2,
    $genre,
    $keyword,
    $identifier,
    $proofreader,
    $curator,
    $grade,
    $gradeop,
    $haspageimages,
    $pagecount,
    $pagecount2,
    $wordcount,
    $wordcount2,
    $corpus,
    $sortField,
    $sortDirection
)
{
    let $start-time := util:system-time()

    let $cached-filter := session:get-attribute("annotation-filter")
    (: The presence of the start parameter indicates we already have a filtered set of results
     : and are paging through it
     :)
    let $filter :=  if ($cached-filter and $start)
                    then $cached-filter
                    else app:build-annotation-filter( $creator, $modifier, $status, () )

    let $cached-annotations := session:get-attribute("filtered-annotations")
    let $selected :=
    if ($cached-annotations and $start) then
        $cached-annotations
    else (

    let $filteredDocuments :=
        app:get-filtered-documents(
            $author,
            $title,
            $date,
            $date2,
            $genre,
            $keyword,
            $identifier,
            $proofreader,
            $curator,
            $grade,
            $gradeop,
            $haspageimages,
            $pagecount,
            $pagecount2,
            $wordcount,
            $wordcount2,
            $corpus,
            $sortField,
            $sortDirection,
            false()
        )

    (: If we have documents from the text filter, we'll iterate over those and select
     : annotatations from the related annotation files in an inner loop.  Otherwise
     : we'll iterate over all annotations in the annotation collection that meet
     : the annotation filter criteria, if any.
     :)
    let $query_prologue :=
        if (exists($filteredDocuments))
        then 'for $doc at $position in $filteredDocuments '
                || 'let $annodoc := substring-before(util:document-name($doc), ".xml") || "_annotations.xml" '
                || 'let $annodocpath := $config:annotation-root || "/" || substring($annodoc, 1, 3) || "/" || $annodoc '
                || 'for $anno in doc($annodocpath)'
        else
            'for $anno in collection("' || $config:annotation-root || '")'

    (: The documents are already sorted if a document filter has been specified, so preserve
     : their order by making the position captured in the "at" clause of the outer loop the
     : first order by criterion.
     :)
    let $docOrder := if (exists($filteredDocuments)) then '$position, ' else ''
    (: The rest of the ordering is done by the word ID of where the annotation starts.  That
     : makes adjacent annotations in the list serve as additional context for each other if
     : they are close and in any case seems the most logical sort order.
     :)
    let $order :=
        ' let  $word := if ($anno//target-selector/@type eq "RangeSelector") then $anno//start-selector/@value/string() else $anno//target-selector/@value/string()'
        || ' group by ' || $docOrder || ' $word'
        || ' order by ' || $docOrder || ' $word'

    (: We sometimes reduce the number of texts available to reduce crash recovery time, so we also
     : need to restrict the Review page to show only annotations for which the texts exist in the
     : database; otherwise we crash the review page by trying to generate context from documents
     : that don't exist.  Not needed when we already have a document filter.
     :)
    let $word_group := '&lt;annotated-word id="{$word}"&gt;{for $a in $anno return $a}&lt;/annotated-word&gt;'

    let $query_return := if (exists($filteredDocuments))
                         then ' ' || $word_group
                         else
                         ' let $docname := $anno[1]//annotation-target/@source/string() || ".xml"'
                         || ' let $docpath := $config:data-root || "/" || substring($docname, 1, 3) || "/" || $docname '
                         || ' return if (fn:doc-available($docpath)) then '
                         || $word_group
                         || ' else ()'

    let $query := $query_prologue || '//annotation-item[' || $filter || ']' || $order || ' return' || $query_return
    let $x := console:log("$query: " || $query)
    let $result := util:eval($query)
    return $result
    )
    let $seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
    let $x := console:log("Found " || count($selected) || " annotated words in " || $seconds || " seconds.")

    return
    (
        session:set-attribute("filtered-annotations", $selected),
        session:set-attribute("annotation-filter", $filter),
        session:set-attribute("creator", $creator),
        session:set-attribute("modifier", $modifier),
        session:set-attribute("status", $status),
        map
        {
            "all" : $selected
        }
    )
};

(:
 : This is called from ajax.xql to feed the current page's annotations to the Polymer client.
 :)
declare function app:get-page-annotations() {
    let $annotations := session:get-attribute("page-annotations")
    let $x := console:log('got ' || xs:string(count($annotations)) || " annotations on page.")

    return $annotations
};

declare
    %templates:wrap
    %templates:default("start", 1)
    %templates:default("per-page", 10)
function app:annotation-review-hits($node as node(), $model as map(*), $start as xs:int, $per-page as xs:int) {
    let $annotated-words := subsequence($model?all, $start, $per-page)
    let $x := session:set-attribute("page-annotations", $annotated-words/annotation-item)
    return $annotated-words/annotation-item
};

declare
    %templates:wrap
    %templates:default("start", 1)
    %templates:default("per-page", 10)
function app:browse-annotations($node as node(), $model as map(*), $start as xs:int, $per-page as xs:int, $extendedfilter as xs:string?) {
    if (empty($model?all) and (empty($extendedfilter) or $extendedfilter = "")) then
        templates:process($node/*[@class="empty"], $model)
    else
        subsequence($model?all, $start, $per-page) !
            templates:process($node/*[not(@class="empty")], map:merge(($model, map { "annotation": . })))
};

(:~
 : Create a bootstrap pagination element to navigate through the hits.
 :)
declare
    %templates:default('key', 'hits')
    %templates:default('start', 1)
    %templates:default("per-page", 10)
    %templates:default("min-hits", 0)
    %templates:default("max-pages", 10)
function app:paginate($node as node(), $model as map(*), $key as xs:string, $start as xs:int, $per-page as xs:int, $min-hits as xs:int,
    $max-pages as xs:int) {
    if ($min-hits < 0 or count($model($key)) >= $min-hits) then
        element { node-name($node) } {
            $node/@*,
            let $count := xs:integer(ceiling(count($model($key))) div $per-page) + 1
            let $middle := ($max-pages + 1) idiv 2
            return (
                if ($start = 1) then (
                    <li class="disabled page-item">
                        <a class="material-icons page-link">first_page</a>
                    </li>,
                    <li class="disabled page-item">
                        <a class="material-icons page-link">navigate_before</a>
                    </li>
                ) else (
                    <li class="page-item">
                        <a href="?start=1" class="material-icons page-link">first_page</a>
                    </li>,
                    <li class="page-item">
                        <a href="?start={max( ($start - $per-page, 1 ) ) }" class="material-icons page-link">navigate_before</a>
                    </li>
                ),
                let $startPage := xs:integer(ceiling($start div $per-page))
                let $lowerBound := max(($startPage - ($max-pages idiv 2), 1))
                let $upperBound := min(($lowerBound + $max-pages - 1, $count))
                let $lowerBound := max(($upperBound - $max-pages + 1, 1))
                for $i in $lowerBound to $upperBound
                return
                    if ($i = ceiling($start div $per-page)) then
                        <li class="active page-item"><a class="page-link" href="?start={max( (($i - 1) * $per-page + 1, 1) )}">{$i}</a></li>
                    else
                        <li class="page-item"><a class="page-link" href="?start={max( (($i - 1) * $per-page + 1, 1)) }">{$i}</a></li>,
                if ($start + $per-page < count($model($key))) then (
                    <li class="page-item">
                        <a href="?start={$start + $per-page}" class="material-icons page-link">navigate_next</a>
                    </li>,
                    <li class="page-item">
                        <a href="?start={max( (($count - 1) * $per-page + 1, 1))}" class="material-icons page-link">last_page</a>
                    </li>
                ) else (
                    <li class="disabled page-item">
                        <a class="material-icons page-link">navigate_next</a>
                    </li>,
                    <li class="disabled page-item">
                        <a class="material-icons page-link">last_page</a>
                    </li>
                )
            )
        }
    else
        ()
};

(:~
    Create a span with the number of items in the current search result.
:)
declare
    %templates:wrap
    %templates:default("key", "hitCount")
function app:hit-count($node as node()*, $model as map(*), $key as xs:string) {
    let $value := $model?($key)
    return
        if ($value instance of xs:integer) then
            $value
        else
            count($value)
};

declare
    %templates:wrap
    %templates:default("key", "annotationHitCount")
function app:annotation-hit-count($node as node()*, $model as map(*), $key as xs:string) {
    let $value := $model?($key)
    return
        if ($value instance of xs:integer) then
            $value
        else
            count($value)
};



(:~
    Create a span with the maximum number of items displayed from the current search result.
:)
declare
    %templates:wrap
    %templates:default("key", "hitsDisplayed")
function app:hits-displayed($node as node()*, $model as map(*), $key as xs:string) {
    let $value := $model?($key)
    return
        if ($value instance of xs:integer) then
            $value
        else
            count($value)
};

(:~
 : Get work title.
 :)
declare function app:work-title($node as node(), $model as map(*), $type as xs:string?) {
    let $suffix := if ($type) then "." || $type else ()
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $id := util:document-name($work)
    return
        <a href="{$node/@href}{$id}{$suffix}">{ app:work-title($work) }</a>
};

declare %private function app:work-title($work as element(tei:TEI)?) {
    let $main-title := $work//ep:title/text()
    let $main-title := if ($main-title) then $main-title else $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main']/text()
    let $main-title := if ($main-title) then $main-title else $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]/text()
    return
        $main-title
};

(:~
 : Check if work has facsimile element indicating page images available.
 :)
declare function app:work-hasPageImages($node as node(), $model as map(*), $type as xs:string?)
{
    let $work := $model("work")/ancestor-or-self::tei:TEI
    return app:work-hasPageImages($work)
};

declare %private function app:work-hasPageImages($work as element(tei:TEI)?)
{
     let $hasPageImages := if ( count( $work//tei:facsimile ) > 0 )
     then
        let $image-source := $work//tei:facsimile/tei:surfaceGrp/tei:note[@type="witnessDetail"]/string()
        return
            if (string-length($image-source) > 0)
            then
                "from " || $image-source
            else
                "yes"
     else "no"
     return $hasPageImages
};

declare %private function app:work-date($work as element(tei:TEI)?)
{
    let $workdate := $work//ep:creationYear/text()
    let $workdate := if (string-length($workdate) > 0) then $workdate else $work//ep:publicationYear/text()
    let $workdate := if (string-length($workdate) > 0) then $workdate else $work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull[@n="printed source"]/tei:publicationStmt/tei:date[@type="creation_date"]/text()
    let $workdate := if ($workdate) then $workdate else $work/tei:teiHeader/tei:fileDesc/tei:editionStmt/tei:edition/tei:date/text()
    let $workdate := if ($workdate) then $workdate else $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:date/text()
    return $workdate
};

declare function app:download-link($node as node(), $model as map(*), $type as xs:string, $doc as xs:string?,
    $source as xs:boolean?) {
    let $file :=
        if ($model?work) then
            replace(util:document-name($model("work")), "^(.*?)\.[^\.]*$", "$1")
        else
            replace($doc, "^(.*)\..*$", "$1")
    let $uuid := util:uuid()

    let $extension := switch ($type)
    case 'stripped'
        return '_unadorned.xml.zip'
    case 'tei'
        return '.xml.zip'
    default
        return '.' || $type

    let $href := switch ($type)
    case 'plain'
        return $node/@href || $file || $extension || "?token=" || $uuid || "&amp;cache=no" || (if ($source) then "&amp;source=yes" else ())
    default
        return "/downloads/" || substring($file, 1, 3) || "/" || $file || $extension

    return
        element { node-name($node) } {
            $node/@*,
            if ($type eq 'plain') then attribute data-token { $uuid } else (),
            attribute href { $href },
            if ($type ne 'plain') then attribute download {} else (),
            $node/node()
        }
};

declare
    %templates:replace
function app:get-filter-string ($node as node()*, $model as map(*)) as element()* {
    let $filter := session:get-attribute("filter")
    return
        if (empty($filter) or $filter = "") then ()
        else (
            <small>filtered by
                {session:get-attribute("browse")}: {$filter}</small>
        )
};

declare %private function app:get-current($div as element()?) {
    if (empty($div)) then
        ()
    else
        if ($div instance of element(tei:teiHeader)) then
        $div
        else
            if (
                empty($div/preceding-sibling::tei:div)  (: first div in section :)
                and count($div/preceding-sibling::*) < 5 (: less than 5 elements before div :)
                and $div/.. instance of element(tei:div) (: parent is a div :)
            ) then
                pages:get-previous($div/..)
            else
                $div
};

declare function app:work-author($node as node(), $model as map(*)) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $work-authors := $work//ep:author/ep:name
    let $work-authors := if ($work-authors) then $work-authors else
        $work//ep:author
    let $work-authors := if ($work-authors) then $work-authors else
        $work//tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:titleStmt/tei:author
    return
        string-join($work-authors, "; ")
};

declare %private function app:work-author($work as element(tei:TEI)?)
{
    let $work-authors := $work//ep:author/ep:name
    let $work-authors := if ($work-authors) then $work-authors else
        $work//ep:author
    let $work-authors := if ($work-authors) then $work-authors else
        $work//tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:titleStmt/tei:author
    return
        string-join($work-authors, "; ")
};

declare function app:work-curator($node as node(), $model as map(*)) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $work-curators := $work//ep:curator/ep:name
    return
        string-join($work-curators, "; ")
};

declare %private function app:work-curator($work as element(tei:TEI)?)
{
    let $work-curators := $work//ep:curator/ep:name
    return
        string-join($work-curators, "; ")
};

declare
    %templates:wrap
function app:work-genre($node as node(), $model as map(*))
{
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $work-genre := $work//ep:genre/text()
    return $work-genre
};

declare %private function app:work-genre($work as element(tei:TEI)?)
{
    let $work-genre := $work//ep:genre/text()
    return $work-genre
};

declare
    %templates:wrap
function app:work-subgenre($node as node(), $model as map(*))
{
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $work-subgenre := $work//ep:subgenre/text()
    return $work-subgenre
};

declare %private function app:work-subgenre($work as element(tei:TEI)?)
{
    let $work-subgenre := $work//ep:subgenre/text()
    return $work-subgenre
};

declare
    %templates:wrap
function app:work-edition($node as node(), $model as map(*)) {
    let $edition := $model("work")/ancestor-or-self::tei:TEI/tei:teiHeader/tei:fileDesc/tei:editionStmt/tei:edition/tei:date/text()
    let $edition := if ($edition) then $edition else $model("work")/ancestor-or-self::tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:date/text()
    return
        $edition
};

declare function app:work-filename($node as node(), $model as map(*), $type as xs:string?) {
    let $suffix := if ($type) then "." || $type else ()
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $id := util:document-name($work)
    return
        <a xmlns="http://www.w3.org/1999/xhtml" href="{$node/@href}{$id}{$suffix}">{ app:work-filename($work) }</a>
};

declare %private function app:work-filename($work as element(tei:TEI)?) {
    let $filename := $work/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno/text()
    return $filename
};

declare %private function app:work-proofreader($work as element(tei:TEI)?)
{
    let $work-proofreader := $work//ep:proofReader/ep:name
    return $work-proofreader
};

declare %private function app:work-edition-statement($work as element(tei:TEI)?)
{
 (:  This method cannot currently work because the fields in the epHeader are not
     reliably present.
   
    let $tcp := $work//ep:tcp/text()
    let $estc := $work//ep:estc/text()
    let $stc := $work//ep:stc/text()
 
    return
        <span>This text is an enriched version of the TCP digital transcription {$tcp} 
     of text {$estc} in the <a href="http://estc.bl.uk" target="_blank">English Short Title Catalog</a> {$stc}
         More editorial details are <a href="/shc/about.html" target="_blank">here</a>.
        </span>
:)
    let $e := $work/tei:teiHeader/tei:fileDesc/tei:editionStmt/tei:edition//text()
    return
        <span>{$e}</span>
};


(:
    Get page count for work.
:)

declare %private function app:work-pagecount($work as element(tei:TEI)?)
{
    let $pcount := ft:get-field(document-uri(root($work)), "pagecount" )
    let $pagecount :=
        if ( string-length( $pcount ) ) then
            xs:integer( $pcount )
        else
            count( $work//tei:pb )

    return $pagecount
};

(:
    Get word count for work.
:)

declare %private function app:work-wordcount($work as element(tei:TEI)?)
{
    let $wcount := ft:get-field(document-uri(root($work)), "wordcount" )
    let $wordcount :=
        if ( string-length( $wcount ) ) then
            xs:integer( $wcount )
        else
            count( $work//tei:w )

    return $wordcount
};

declare
    %templates:wrap
function app:work-header($node as node(), $model as map(*))
{
    let $work   := $model("work")/ancestor-or-self::tei:TEI
    let $id     := util:document-name($work)
    let $docid  := substring-before($id, '.xml')  (: much faster than $work/@xml:id :)
    let $work-title        := app:work-title($work)
    let $work-author       := app:work-author($work)
    let $work-date         := app:work-date($work)
    let $camera :=
        if ( app:work-hasPageImages($work) != "no" ) then
            <div id="camera_icon" class="material-icons" data-toggle="tooltip" title="Has page images">photo_camera</div>
        else
            ""
    let $disco_link := "https://earlyprint.org/lab/tool_discovery_engine.html"
                           || "?which_to_do=find_texts&amp;eebo_tcp_id=" || $docid || "&amp;n_results=35"

    let $disco :=
        <a href="{$disco_link}" target="_blank" id="discovery_engine" class="material-icons" data-toggle="tooltip" title="Find similar texts">
            launch
        </a>

    let $work-authorHTML :=
        if ( $work-author != "" ) then
            <span>By {$work-author}.</span>
        else
            ""
    return
       <div>
            <h5><a href="{$node/@href}{$id}">{$work-title}</a>.
                                                    <span style="margin: 5px"/>
                                                    {$work-authorHTML}
                                                    <span style="margin: 5px"/>
                                                    {$work-date}.
                                                    <span style="margin: 5px"/>
                                                    {$camera}
                                                    <span style="margin: 5px"/>
                                                    {$disco}
                                                    <span style="margin: 5px"/>
                <button href="#{$docid}" class="nav-toggle material-icons" title="Show more information">unfold_more</button>
            </h5>
       </div>
};

declare
    %private
function app:get-context-between($start as node(),
                                 $end as node(),
                                 $context as node()*,
                                 $anno as node()*) as node()* {

        if ($start is $end) then
            s2i:wrap-recursive($start, $anno/annotation-item)
        else (
            (: Avoid pathologically slow wildcard axes by getting both w and pc
               and taking the earlier of the two in document order.
             :)
            let $next_w := $start/following::tei:w[1]
            let $next_pc := $start/following::tei:pc[1]
            let $next := if ($next_w << $next_pc) then $next_w else $next_pc

            return
                if (empty($next) or $next is $end)
                then s2i:wrap-recursive(($context, $start, $next), $anno/annotation-item)
                else app:get-context-between($next, $end, ($context, $start, $next), $anno)
        )
};

declare function app:add-annotation-metadata($node as node(), $model as map(*), $doc as xs:string) {
    let $anno := $model("annotation")/annotation-item[1]
    let $id := $anno/@id/string()
    let $source := $anno//annotation-target/@source/string()

    return
    element { node-name($node) } {
        $node/@*,
        attribute id { $id },
        attribute source { $source },
        attribute version { "" },
        templates:process($node/*, $model)
    }
};


declare
    %templates:wrap
function app:work-defects($node as node(), $model as map(*))
{
    let $work  := $model("work")/ancestor-or-self::tei:TEI
    let $docid  := substring-before(util:document-name($work), '.xml')  (: much faster than $work/@xml:id :)

    let $work-pages                := app:work-pagecount( $work )
    let $work-pages                := format-number( $work-pages , "#" )

    let $work-words                := app:work-wordcount( $work )
    let $work-words                := format-number( $work-words , "#" )

    let $untranscribedForeignCount := $work//ep:untranscribedForeignCount/text()
    let $untranscribedMathCount    := $work//ep:untranscribedMathCount/text()
    let $untranscribedMusicCount   := $work//ep:untranscribedMusicCount/text()
    let $defectiveTokenCount       := $work//ep:defectiveTokenCount/text()
    let $missingChunkCount         := $work//ep:missingChunkCount/text()
    let $missingPagesCount         := $work//ep:missingPagesCount/text()
    let $defectRate           := $work//ep:defectRate/text()
    let $finalGrade           := $work//ep:finalGrade/text()
    let $textsPerGrade        := $work//ep:textsPerGrade/text()
    let $defectRangePerGrade  := $work//ep:defectRangePerGrade/text()
    let $work-edition-statement := app:work-edition-statement($work)
    let $work-curator         := app:work-curator($work)
    let $work-proofReader     := app:work-proofreader($work)
    let $work-hasPageImages   := app:work-hasPageImages($work)
    let $work-subgenre        := app:work-subgenre($work)

    let $defectRate           := $defectRate * 1.0

    let $untranscribedForeignCountHTML :=
        if ( $untranscribedForeignCount = "0" ) then
            ""
        else if ( $untranscribedForeignCount = "1" ) then
            <li>{$untranscribedForeignCount} untranscribed word in a non-Roman alphabet.</li>
        else if ( $untranscribedForeignCount != "" ) then
            <li>{$untranscribedForeignCount} untranscribed words in a non-Roman alphabet.</li>
        else
            ""
    let $untranscribedMathCountHTML :=
        if ( $untranscribedMathCount = "0" ) then
            ""
        else if ( $untranscribedMathCount = "1" ) then
            <li>{$untranscribedMathCount} mathematical symbol.</li>
        else if ( $untranscribedMathCount != "" ) then
            <li>{$untranscribedMathCount} mathematical symbols.</li>
        else
            ""
    let $untranscribedMusicCountHTML :=
        if ( $untranscribedMusicCount = "0" ) then
            ""
        else if ( $untranscribedMusicCount = "1" ) then
            <li>{$untranscribedMusicCount} passage of musical notation.</li>
        else if ( $untranscribedMusicCount != "" ) then
            <li>{$untranscribedMusicCount} passages of musical notation.</li>
        else
            ""
    let $defectiveTokenCountHTML :=
        if ( $defectiveTokenCount = "0" ) then
            ""
        else if ( $defectiveTokenCount = "1" ) then
            <li>{$defectiveTokenCount} missing or incompletely transcribed token.</li>
        else if ( $defectiveTokenCount != "" ) then
            <li>{$defectiveTokenCount} missing or incompletely transcribed tokens.</li>
        else
            ""

    let $missingChunkCountHTML :=
        if ( $missingChunkCount = "0" ) then
            ""
        else if ( $missingChunkCount = "1" ) then
            <li>{$missingChunkCount} missing passage.</li>
        else if ( $missingChunkCount != "" ) then
            <li>{$missingChunkCount} missing passages.</li>
        else
            ""

    let $missingPagesCountHTML :=
        if ( $missingPagesCount = "0" ) then
            ""
        else if ( $missingPagesCount = "1" ) then
            <li>{$missingPagesCount} missing page.</li>
        else if ( $missingPagesCount != "" ) then
            <li>{$missingPagesCount} missing pages.</li>
        else
            ""

    let $work-curatorHTML :=
        if ( $work-curator != "" ) then
              <span>Many incompletely or incorrectly transcribed words were
              corrected by {$work-curator}.</span>
        else
            ""

    let $work-proofReaderHTML :=
        if ( $work-proofReader != "" ) then
            <span>Proofread against page images by {$work-proofReader}.</span>
        else
            <span>Unless otherwise noted explicitly, this text has not been fully proofread.</span>

    return
       if ( $defectRate > 0 ) then
       <div id="{$docid}" style="display:none">
           <p>
               Pages: {$work-pages}<br />
               Words: {$work-words}
           </p>
           <p>
               Genre: {$work-subgenre}
           </p>
           <p>
              Page images available: {$work-hasPageImages}
           </p>
           <p>
               {$work-edition-statement}
           </p>
           <p>
              {$work-curatorHTML}
              <br/>
              {$work-proofReaderHTML}
           </p>
           <p>Remaining known defects include:</p>
           <ul>
               {$untranscribedForeignCountHTML}
               {$untranscribedMathCountHTML}
               {$untranscribedMusicCountHTML}
               {$defectiveTokenCountHTML}
               {$missingChunkCountHTML}
               {$missingPagesCountHTML}
           </ul>
           <br/>
           <p>{$defectRangePerGrade}</p>
       </div>
       else
       <div id="{$docid}" style="display:none">
           <p>
               Pages: {$work-pages}<br />
               Words: {$work-words}
           </p>
           <p>
               Genre: {$work-subgenre}
           </p>
           <p>
              Page images available: {$work-hasPageImages}
           </p>
           <p>
               {$work-edition-statement}
           </p>
           <p>
              {$work-curatorHTML}
              <br/>
              {$work-proofReaderHTML}
           </p>
           <p>{$defectRangePerGrade}</p>
       </div>
};
