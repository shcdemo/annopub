xquery version "3.1";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "../../config.xqm";

import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util";
import module namespace odd="http://www.tei-c.org/tei-simple/odd2odd";

declare namespace expath="http://expath.org/ns/pkg";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html5";
declare option output:media-type "text/html";

let $odd := request:get-parameter("odd", ())
let $odd :=
    if ($odd) then
        $odd
    else
        xmldb:get-child-resources($config:odd-root)[starts-with(., "nu")][ends-with(., ".odd")]
let $result :=
    for $source in $odd
    let $odd := doc($config:odd-root || "/" || $source)
    for $module in ("web")
    return
        try {
            for $output in pmu:process-odd(
                odd:get-compiled($config:odd-root, $source),
                $config:output-root,
                $module,
                "../" || $config:output,
                $config:module-config)
            (: pmu:process-odd returns relative path starting in tei-publisher-lib 3.1.0 :)
            let $file := if (starts-with($output?module, $config:output-root))
                         then $output?module
                         else $config:output-root || '/' || $output?module
            let $src := util:binary-to-string(util:binary-doc($file))
            let $compiled := util:compile-query($src, ())
            return
                if ($compiled/error) then
                    <div class="list-group-item-danger">
                        <h5 class="list-group-item-heading">{$file} {$compiled/error/@line}:</h5>
                        <p class="list-group-item-text">{ $compiled/error/string() }</p>
                        <pre class="list-group-item-text">{ $compiled/error/@line }</pre>
                    </div>
                else
                    <div class="list-group-item-success">{$file}</div>
        } catch * {
            <div class="list-group-item-danger">
                <h5 class="list-group-item-heading">Error for output mode {$module}</h5>
                <p class="list-group-item-text">{ $err:code }: { $err:description }</p>
                <p class="list-group-item-text">{ $err:module }[{ $err:line-number }:{ $err:column-number }]</p>
            </div>
        }
return
    <div class="errors">
        <h4>Regenerated XQuery code from ODD files</h4>
        <div class="list-group">
        {
            $result
        }
        </div>
    </div>
