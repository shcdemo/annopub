# AnnoPub

AnnoPub is a TEI Publisher application based on the eXist XML database for
displaying and searching the texts in the EarlyPrint corpus and enabling
crowd-sourced corrections of those texts via its annotation features. Reading,
searching, and correcting are integrated as much as possible.  The application
could be adapted to other corpora with relative ease.

## Technical Notes

The main eXist application is called (for historical reasons arising from the
precursor project Shakespeare His Contemporaries) 'shc' and makes use of another
separate repository called 'annotation-client'. `annotation-client` contains the
client-side code implementing the UI of AnnoPub's annotation features. It is
built upon [Polymer](http://polymer-project.org) which is an implementation of
W3C Web Components.

The AnnoPub application uses `npm` and `rollup` to incorporate the
`annotation-client` module and other **JavaScript** and **CSS** libraries (such
as e.g. **OpenSeaDragon** for the image display).

Deploying a complete installation of the **Library** is a significant
undertaking and is described more fully in the [EarlyPrint Library Configuration
and Deployment Cookbook](doc/eplib_cookbook.md).

## Building shc

### Requirements

To build xar applications you need:

* Java >= 1.8
* Apache Ant
 
For Javascript:

* nodejs


### Building xar

In general eXistdb apps are built with Apache Ant which essentially compresses
all needed components of the the app into one archive file with the ending
'.xar'.

To build the xar file just execute:

`
ant
`

### Application Runtime Dependencies

The AnnoPub application manages its runtime dependencies (JavaScript and CSS)
via npm. The dependencies are listed in the file 'package.json'.

The annotation-client module is one of these dependencies. It lives in its own
repository and is pulled into AnnoPub whenever the application is built.

### Email Server

For the self-registration of users an email server is needed to send
confirmation when a user registers in the application.  To configure the mail
functionality, edit the file `shc/components/xqmail/xqmail-config.xqm`, setting
the following parameters:

* $xqmail-config:FROM - the mail user connecting to the email server

* $xqmail-config:PASSWORD - the password to authenticate to the email server

* $xqmail-config:CC - optional cc address if you want to get copies of user
  registrations somewhere

* $xqmail-config:BCC - optional blind copies to be sent to another address

Further down in the file a few more properties are defined for the email server
configuration.  The smtp host entry, which looks like this:

```xml
 <property name="mail.smtp.host" value="smtp.gmail.com"/>
```

is the only one that needs to be changed; set the value to the name of your SMTP
server. The other parameters should be self-explanatory and normally do not need
to be changed.

In order to build the application, you must first create the file
`../xqmail.pwd` containing the password of the mail server you will be using for
registration. (Just make a file with one blank line if you will not be
processing registrations.)


### BlackLab

To use **BlackLab** searches, you need to have an external **BlackLab** index
containing the same texts provided by the **Library**.  If the URL for the
**BlackLab** server changes, you will need to edit the line begining with `const
target` in `resources/scripts/highlighting.js` and the `endpoint` attribute of
the `eplib-page` element in `templates/browse.html` to reflect the updated URL.





